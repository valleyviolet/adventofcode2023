#!/usr/bin/env python
# encoding: utf-8
"""
By Eva Schiffer
for the advent of code
"""

import numpy, re

file_obj = open("./input/day3_input.txt")
data = file_obj.readlines()

DIGITS = set(['1', '2', '3', '4', '5', '6', '7', '8', '9', '0',])
NOT_DOT_EXP = r"(?!\.)."
NUMBERS_EXP = r'\d+'

def find_all_with_positions (expression, to_look_in, exclude=DIGITS,) :
    """
    Given an expression and a string to search, find the positions and matches that aren't in the exclude list.
    :param expression:
    :param to_look_in:
    :param exclude:
    :return:
    """

    found = set()

    for foundThing in re.finditer(expression, to_look_in) :
        match_txt = foundThing.group()
        if match_txt not in exclude :
            found.add((foundThing.start(), match_txt))

    return found

def generate_surrounding_idxes (line_idx, col_idx, ) :
    """
    Given a line and column index, return the tuples of all the surrounding indexes,
    including the input index and any directly or diagonally adjacent.

    :param line_idx:
    :param col_idx:
    :return:
    """

    to_return = set( )
    for l_idx in range(line_idx-1, line_idx+2,) :
        for c_idx in range(col_idx-1, col_idx+2,) :
            to_return.add ((l_idx, c_idx,))

    return to_return

# Parse input to find
#       all numbers and their starting positions
#       the positions of any "symbols" and the spaces around them

line_idx = 0
all_nums = set()
adjacent_spaces = set()

# loop through all lines
for line_txt in data :

    # grab any numbers
    tmp_nums = find_all_with_positions(NUMBERS_EXP, line_txt, exclude=set(), )
    for num_info in tmp_nums :
        col_idx, num_txt = num_info
        all_nums.add((line_idx, col_idx, num_txt,))

    # grab any symbols
    tmp_sym = find_all_with_positions(NOT_DOT_EXP, line_txt, exclude=DIGITS, )
    for sym_info in tmp_sym :
        col_idx, sym_txt = sym_info
        print("Found symbol at : " + str(line_idx) + ", " + str(col_idx))
        around_tmp = generate_surrounding_idxes(line_idx, col_idx)
        adjacent_spaces = adjacent_spaces.union(around_tmp)

    line_idx += 1

print("Found numbers: " + str(all_nums) + "\n")
print("Found symbol adjacency: " + str(adjacent_spaces))

# For each number we found, check to see if it's adjacent to a symbol
# and add the number to our sum if it is.
total_vals = 0
for f_l_idx, f_c_idx, f_num in sorted(all_nums) :
    do_use = False

    #check all the spaces in our number for adjacency
    for c_idx in range(f_c_idx, f_c_idx+len(f_num), ) :
        if (f_l_idx, c_idx,) in adjacent_spaces :
            do_use = True

    print("Using number " + str(f_num) + "? " + str(do_use))
    if do_use :
        total_vals += int(f_num)

print("Total sum: " + str(total_vals))

"""
With my inputs: 520135

Time for part 1: 1:55
"""