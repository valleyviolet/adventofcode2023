#!/usr/bin/env python
# encoding: utf-8
"""
By Eva Schiffer
for the advent of code
"""

import re, sys
import numpy

#print("Args: " + str(sys.argv))
day_txt = "10"
file_to_open = "./input/day"+day_txt+"_input_"+sys.argv[1]+".txt" if len(sys.argv) > 1 else "./input/day"+day_txt+"_input.txt"
file_obj = open(file_to_open)
data = file_obj.readlines()

NUMBERS_EXP = r'-?\d+' # ? is zero or 1 times, so numbers can now be negative

# Parse input to find
#       a map of pipes

"""
Pipes may include:

| is a vertical pipe connecting north and south.
- is a horizontal pipe connecting east and west.
L is a 90-degree bend connecting north and east.
J is a 90-degree bend connecting north and west.
7 is a 90-degree bend connecting south and west.
F is a 90-degree bend connecting south and east.
. is ground; there is no pipe in this tile.
S is the starting position; 
    there is a pipe on this tile, but your sketch 
    doesn't show what shape the pipe has.
"""

NORTH = "north"
SOUTH = "south"
EAST  = "east"
WEST  = "west"
CONNECTS_TO =   {
                    '|':    {NORTH, SOUTH,},
                    '-':    {EAST,  WEST, },
                    'L':    {NORTH, EAST, },
                    'J':    {NORTH, WEST, },
                    '7':    {SOUTH, WEST, },
                    'F':    {SOUTH, EAST, },
                    'S':    {NORTH, SOUTH, EAST, WEST}, # start is a sort of wildcard
                }

"""
We are going to do indexing as (line, column). 
With how the current data is postulated, this means line is east/west and column is north/south. 
So: 
    line        - direction = north
    line        + direction = south
    column      - direction = west
    column      + direction = east
"""
TO_GO = {
            NORTH:  (-1,  0),
            SOUTH:  ( 1,  0),
            WEST:   ( 0, -1),
            EAST:   ( 0,  1),
        }
R_TO_GO =   {
                (-1,  0):   NORTH,
                ( 1,  0):   SOUTH,
                ( 0, -1):   WEST,
                ( 0,  1):   EAST,
            }

def find_all_with_positions (expression, to_look_in, exclude=None,) :
    """
    Given an expression and a string to search, find the positions and matches that aren't in the exclude list.
    :param expression:
    :param to_look_in:
    :param exclude:
    :return:
    """

    if exclude is None :
        exclude = set()

    found = set()

    for foundThing in re.finditer(expression, to_look_in) :
        match_txt = foundThing.group()
        if match_txt not in exclude :
            found.add((foundThing.start(), match_txt))

    return found

# parse our input
initial_map = numpy.zeros((len(data), len(data[0].strip())), dtype=numpy.str_,)
line_idx = 0
s_line, s_col = None, None
for line_txt in data :
    if "S" in line_txt :
        s_line = line_idx
        tmp = find_all_with_positions("S", line_txt,)
        s_col = list(tmp)[0][0]
    initial_map[line_idx,:] = list(line_txt.strip())
    line_idx += 1

print("Start at: " + str(s_line) + " " + str(s_col))

def print_map(map_array, indent=0, ) :
    """
    Given a map of the pipes, print it in a user readable way.
    :param map_array:
    :return:
    """

    indent_txt = ""
    for i in range(indent) :
        indent_txt += "\t"

    for l_idx in range(map_array.shape[0]) :
        this_line = indent_txt
        for c_idx in range(map_array.shape[1]) :
            this_line += str(map_array[l_idx,c_idx])
        print(this_line)

print("Input map " + str(initial_map.shape) + ": ")
print_map(initial_map, indent=1,)

def get_connected_positions(line_idx, col_idx, map,) :
    """
    Given a line and column, return the coordinates of all the connected tiles
    that are connected on both ends.
    :param line_idx:
    :param col_idx:
    :return:
    """

    fully_connected = set( )

    outward_connections = get_connected_out(line_idx, col_idx, map,)

    for tmp_position in outward_connections :
        tmp_connections = get_connected_out(tmp_position[0], tmp_position[1], map,)
        if (line_idx, col_idx) in tmp_connections :
            fully_connected.add(tmp_position,)

    return fully_connected

def get_connected_out(line_idx, col_idx, map,) :
    """
    Given a line and column, return the coordinates of all it's outward connections
    (Note: those might not connect on the other side.)
    :param line_idx:
    :param col_idx:
    :param map:
    :return:
    """
    here_char = map[line_idx, col_idx]
    connected = set()

    if here_char in CONNECTS_TO:
        for direction in CONNECTS_TO[here_char]:
            tmp_offset_line, tmp_offset_col = TO_GO[direction]
            connected.add((line_idx + tmp_offset_line, col_idx + tmp_offset_col),)

    return connected

def calc_s_type(s_line, s_col, s_connections, ) :
    """
    Given where the start is and what it's connected to, figure out what
    type of pipe it is.
    :param s_line:
    :param s_col:
    :param s_connections:
    :return:
    """
    diffs_set = set()
    for tmp_coords in s_connections :
        line_d = tmp_coords[0] - s_line
        col_d  = tmp_coords[1] - s_col
        diffs_set.add(R_TO_GO[(line_d, col_d)])
    to_return = None
    for p_type in CONNECTS_TO :
        if p_type != 'S' and diffs_set.issubset(CONNECTS_TO[p_type]) :
            to_return = p_type

    return to_return

# figure out where the loop of pipes we care about is
our_loop = { (s_line, s_col), }

# start by getting the actual connections for the start pipe
curr_connections = get_connected_positions(s_line, s_col, initial_map,)
print("Initial connections to \"S\": " + str(curr_connections))
s_type = calc_s_type(s_line, s_col, curr_connections,)
print("S is type: " + s_type)
# loop through until we stop finding new connections
while not curr_connections.issubset(our_loop) :
    new_curr_connections = set( )
    for c_line_idx, c_col_idx in curr_connections :
        our_loop.add((c_line_idx, c_col_idx),)
        tmp = get_connected_positions(c_line_idx, c_col_idx, initial_map,)
        #print("found: " + str(tmp))
        new_curr_connections = new_curr_connections.union(tmp)
        #print("new: " + str(new_curr_connections))
    curr_connections = new_curr_connections - our_loop # we only need to look at the new ends of the path
print("Found " + str(len(our_loop)) + " tiles in our loop.")

# make ourselves a nice clean map
clean_map = initial_map.copy()
for l_idx in range(clean_map.shape[0]) :
    for c_idx in range(clean_map.shape[1]) :
        if (l_idx, c_idx) not in our_loop :
            clean_map[l_idx, c_idx] = "."
clean_map[s_line, s_col] = s_type

print("Clean version of the map: ")
print_map(clean_map, indent=1,)

# figure out how many spaces are inside the loop
count_inside = 0

IS_INTERSECTION = { '|',  'L', 'J', '7', 'F' }
# note, '-',

# note, we're going to do this by using a ray casting algorithm looking across each line
# see: https://en.wikipedia.org/wiki/Point_in_polygon#Ray_casting_algorithm
for l_idx in range(clean_map.shape[0]) :
    intersection_count = 0
    open_pair = None
    for c_idx in range(clean_map.shape[1]) :
        here_char = clean_map[l_idx, c_idx]
        if here_char == '|' :
            intersection_count += 1
        elif here_char == 'L' or here_char == 'F' :
            open_pair = here_char
        elif here_char == '7' or here_char == 'J' :
            if open_pair == 'L' :
                open_pair = None
                if here_char == '7':
                    intersection_count += 1
            elif open_pair == 'F' :
                open_pair == None
                if here_char == 'J' :
                    intersection_count += 1
        elif here_char == '.' :
            count_inside = count_inside + 1 if intersection_count % 2 == 1 else count_inside

print("Spaces inside the loop: " + str(count_inside))

"""
With my inputs: 371

Time for part 1: 2:13
Time for part 2: 0:48
Total time for this day: 3:01
"""