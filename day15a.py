#!/usr/bin/env python
# encoding: utf-8
"""
By Eva Schiffer
for the advent of code
"""

import re, sys, os

NUMBERS_EXP = r'-?\d+' # ? is zero or 1 times, so numbers can now be negative

#print("Args: " + str(sys.argv))
day_txt = re.findall(NUMBERS_EXP, os.path.basename(vars(sys.modules[__name__])['__file__']))[0]
file_to_open = "./input/day"+day_txt+"_input_"+sys.argv[1]+".txt" if len(sys.argv) > 1 else "./input/day"+day_txt+"_input.txt"
file_obj = open(file_to_open)
data = file_obj.readlines()

# Parse input to find
#       a comma separated list of instructions

# note, we're ignoring new lines, commas, and (I think also) outer white space.

# parse our input
commands_txt = data[0].strip().split(',')

def holiday_hash (string_input,) :
    """
    Given a string, return the results of the "Holiday ASCII String Helper algorithm" on that string.

    Documentation for this algorithm:

        To run the HASH algorithm on a string, start with a current value of 0.
        Then, for each character in the string starting from the beginning:

            Determine the ASCII code for the current character of the string.
            Increase the current value by the ASCII code you just determined.
            Set the current value to itself multiplied by 17.
            Set the current value to the remainder of dividing itself by 256.

        After following these steps for each character in the string in order,
        the current value is the output of the HASH algorithm.

    :param string_input:
    :return: a single number in the range 0 to 255
    """
    curr_value = 0

    for char_tmp in list(string_input) :
        curr_value += ord(char_tmp)
        curr_value *= 17
        curr_value %= 256

    return curr_value

# visualize input to make sure we parsed it correctly
#print("input data: " + str(commands_txt))

# minimal test of our hash
#print("Hash of \'HASH\': " + str(holiday_hash('HASH')))

# hash all our input data
hash_sum = 0
for command in commands_txt :
    #print("Hashing text: " + command)
    tmp_hash = holiday_hash(command)
    #print("\thash: " + str(tmp_hash))
    hash_sum += tmp_hash

print("Total hash sum: " + str(hash_sum))

"""
With my inputs: 

Time for part 1: 0:12
"""