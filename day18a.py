#!/usr/bin/env python
# encoding: utf-8
"""
By Eva Schiffer
for the advent of code
"""

import re, sys, os

NUMBERS_EXP = r'-?\d+' # ? is zero or 1 times, so numbers can now be negative

#print("Args: " + str(sys.argv))
day_txt = re.findall(NUMBERS_EXP, os.path.basename(vars(sys.modules[__name__])['__file__']))[0]
file_to_open = "./input/day"+day_txt+"_input_"+sys.argv[1]+".txt" if len(sys.argv) > 1 else "./input/day"+day_txt+"_input.txt"
file_obj = open(file_to_open)
data = file_obj.readlines()

# Parse input to find
#       a list of dig commands
#           direction (U D L or R)
#           number of spaces
#           an rgb color code

"""
The direction is U, D, L, or R which corresponds to UP DOWN LEFT RIGHT but also basically
    UP    = North
    DOWN  = South
    LEFT  = West
    RIGHT = East

The number is how many spaces we will dig in the indicated direction.

The color is the color we're planning to paint that side of the lagoon when it's finished.
"""

UP    = "U"
DOWN  = "D"
LEFT  = "L"
RIGHT = "R"
TO_GO = {
            UP:     (-1,  0),
            DOWN:   ( 1,  0),
            LEFT:   ( 0, -1),
            RIGHT:  ( 0,  1),
        }

# parse our input
tmp_ln_num = 0
tmp_col_num = 0
all_dug  = set( )
all_lns  = set( )
all_cols = set( )
for ln_txt in data :
    tmp = ln_txt.split(" ")
    dir = tmp[0]
    num = int(tmp[1])
    tmp_ln_inc  = TO_GO[dir][0]
    tmp_col_inc = TO_GO[dir][1]
    for x in range(num) :
        tmp_ln_num  += tmp_ln_inc
        tmp_col_num += tmp_col_inc
        all_dug.add((tmp_ln_num, tmp_col_num),)
        all_lns.add(tmp_ln_num)
        all_cols.add(tmp_col_num)
tmp_lines = sorted(list(all_lns))
min_line = tmp_lines[0]
max_line = tmp_lines[-1]
tmp_cols = sorted(list(all_cols))
min_col  = tmp_cols[0]
max_col  = tmp_cols[-1]

def print_map(stuff_positions, min_line, max_line, min_col, max_col, indent=0, true_char='#', false_char='.',
              extra_stuff=None, extra_stuff_char="@"  ) :
    """
    Given a range of our indexes (line, col) and the position that stuff is, print out our map
    Note: indexes may be less than zero

    :param indent:
    :param true_char:
    :param false_char:
    :param stuff_positions:
    :return:
    """
    extra_stuff = set( ) if extra_stuff is None else extra_stuff

    indent_txt = ""
    for i in range(indent) :
        indent_txt += "\t"

    for l_idx in range(min_line, max_line+1,) :
        this_line = indent_txt
        for c_idx in range(min_col, max_col+1,) :
            tmp_char = true_char if (l_idx, c_idx) in stuff_positions else false_char
            tmp_char = extra_stuff_char if (l_idx, c_idx) in extra_stuff else tmp_char
            this_line += tmp_char
        print(this_line)

# visualize input to make sure we parsed it correctly

print("Our basic hole: ")
print_map(all_dug, min_line, max_line, min_col, max_col, indent=1,)
print("Found data range (" + str(min_line) + " to " + str(max_line) + ", " + str(min_col) + " to " + str(max_col) + ")")
print("Dug out " + str(len(list(all_dug))) + " cubic meters.")

def get_flood_fill_spaces(fill_start_postion, edge_hole_positions, min_line, max_line, min_col, max_col,) :
    """
    Use a polygon intersection algorithm to find all the spots inisde the hole.

    :param fill_start_postion:
    :param edge_hole_positions:
    :param min_line:
    :param max_line:
    :param min_col:
    :param max_col:
    :return:
    """
    filled_spaces = {fill_start_postion,}
    num_filled = 0

    while len(filled_spaces) > num_filled :
        num_filled = len(filled_spaces)

        this_time = set( )
        for tmp_position in filled_spaces :
            for l_inc in range(-1, 2) :
                for c_inc in range(-1, 2) :
                    new_pos = (tmp_position[0] + l_inc, tmp_position[1] + c_inc)
                    if new_pos not in edge_hole_positions and new_pos not in filled_spaces :
                        this_time.add(new_pos)
        filled_spaces = filled_spaces.union(this_time)

    return filled_spaces

# dig out the rest of our hole
new_fill = get_flood_fill_spaces((1,1), all_dug, min_line, max_line, min_col, max_col,) # note (1, 1) as a start isn't a general solution, but it works for my input and the test data
print("Fully filled map: ")
print_map(all_dug, min_line, max_line, min_col, max_col, indent=1, extra_stuff=new_fill,)

print("Number of dug square meters: " + str(len(all_dug.union(new_fill))))

"""
With my inputs: 45159
"""