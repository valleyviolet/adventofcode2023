#!/usr/bin/env python
# encoding: utf-8
"""
By Eva Schiffer
for the advent of code
"""

import re, sys
import numpy

#print("Args: " + str(sys.argv))
day_txt = "11"
file_to_open = "./input/day"+day_txt+"_input_"+sys.argv[1]+".txt" if len(sys.argv) > 1 else "./input/day"+day_txt+"_input.txt"
file_obj = open(file_to_open)
data = file_obj.readlines()

# Parse input to find
#       a map of galaxies in the sky

# galaxies are represented by '#' and empty sky is represented by '.'

def find_all_with_positions (expression, to_look_in, exclude=None,) :
    """
    Given an expression and a string to search, find the positions and matches that aren't in the exclude list.
    :param expression:
    :param to_look_in:
    :param exclude:
    :return:
    """

    if exclude is None :
        exclude = set()

    found = set()

    for foundThing in re.finditer(expression, to_look_in) :
        match_txt = foundThing.group()
        if match_txt not in exclude :
            found.add((foundThing.start(), match_txt))

    return found

# parse our input
map_shape = (len(data), len(data[0].strip()))
#is_galaxy = numpy.zeros(map_shape, dtype=bool,)
galaxy_positions = set( )
l_counts = { } # how many galaxies are on a line?
c_counts = { } # how many galaxies are in a a column?
line_idx = 0
for line_txt in data :
    #is_galaxy[line_idx,:] = numpy.array(list(line_txt.strip()), dtype=numpy.str_) == '#'
    tmp_set = find_all_with_positions(r'#', line_txt,)
    for info in tmp_set :
        galaxy_positions.add((line_idx, info[0],))
        l_counts[line_idx] = l_counts[line_idx] + 1 if line_idx in l_counts else 1
        c_counts[info[0]]  = c_counts[info[0]]  + 1 if info[0]  in c_counts else 1
    line_idx += 1

def print_map(map_shape, stuff_positions, indent=0, ) :
    """
    Given a map of shape map_shape with stuff at stuff_positions, print it in a user readable way.
    :param map_shape
    :param stuff_positions Where important things are, the rest is blank
    :return:
    """

    indent_txt = ""
    for i in range(indent) :
        indent_txt += "\t"

    for l_idx in range(map_shape[0]) :
        this_line = indent_txt
        for c_idx in range(map_shape[1]) :
            this_line += '*' if (l_idx, c_idx) in stuff_positions else '.'
        print(this_line)

print("Input map " + str(map_shape) + ": ")
print_map(map_shape, galaxy_positions, indent=1,)
print("Starting galaxy positions: " + str(galaxy_positions))

def expand_universe (map_shape, galaxy_positions, line_counts, column_counts,) :
    """
    Expand any lines or rows in our view that are empty.
    :param map_shape:
    :param galaxy_positions:
    :return:
    """
    shape_to_return = map_shape
    positions_to_return = galaxy_positions.copy()

    # expand lines
    lines_to_double = [ ]
    for l_idx in range(map_shape[0]) :
        if l_idx not in line_counts or line_counts[l_idx] <= 0 :
            lines_to_double.append(l_idx)
    #print("lines to double: " + str(lines_to_double))
    # go through the lines (largest first) and increase our numbers accordingly
    for l_idx in reversed(sorted(lines_to_double)) :
        tmp_positions = set( )
        shape_to_return = (shape_to_return[0] + 1, shape_to_return[1])
        for position in positions_to_return :
            new_ln_num = position[0] if position[0] < l_idx else position[0] + 1
            tmp_positions.add((new_ln_num, position[1]))
        positions_to_return = tmp_positions

    # expand columns
    columns_to_double = [ ]
    for c_idx in range(map_shape[1]) :
        if c_idx not in column_counts or column_counts[c_idx] <= 0 :
            columns_to_double.append(c_idx)
    #print("columns to double: " + str(columns_to_double))
    # go through the columns (largest first) and increase our numbers accordingly
    for c_idx in reversed(sorted(columns_to_double)) :
        tmp_positions = set( )
        shape_to_return = (shape_to_return[0], shape_to_return[1] + 1)
        for position in positions_to_return :
            new_col_num = position[1] if position[1] < c_idx else position[1] + 1
            tmp_positions.add((position[0], new_col_num))
        positions_to_return = tmp_positions

    return shape_to_return, positions_to_return

# expand our map where there is empty space
new_map_shape, new_galaxy_positions = expand_universe(map_shape, galaxy_positions, l_counts, c_counts,)

print("Expanded version of the map " + str(new_map_shape) + ": ")
print_map(new_map_shape, new_galaxy_positions, indent=1,)

# calculate all our galaxy distances
sum_of_distances = 0
for galaxy_pt_A in new_galaxy_positions :
    A_line, A_col = galaxy_pt_A
    for galaxy_pt_B in new_galaxy_positions :
        B_line, B_col = galaxy_pt_B
        # don't compare a point to itself
        if not((A_line == B_line) and (A_col == B_col)) :
            # calculate the distance
            tmp_dist = abs(A_line - B_line) + abs(A_col - B_col)
            sum_of_distances += tmp_dist
# this is going to result in comparing all points twice, so divide by two
sum_of_distances = int(sum_of_distances/2)

print("Sum of galaxy distances: " + str(sum_of_distances))

"""
With my inputs: 9648398

Time for part 1: 1:00
"""