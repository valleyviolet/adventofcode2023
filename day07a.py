#!/usr/bin/env python
# encoding: utf-8
"""
By Eva Schiffer
for the advent of code
"""

import re, sys
import numpy
from collections import Counter
from functools import cmp_to_key

#print("Args: " + str(sys.argv))
day_txt = "7"
file_to_open = "./input/day"+day_txt+"_input_"+sys.argv[1]+".txt" if len(sys.argv) > 1 else "./input/day"+day_txt+"_input.txt"
file_obj = open(file_to_open)
data = file_obj.readlines()

NUMBERS_EXP = r'\d+'

# Parse input to find
#       a hand of cards (string of 5 cards that are "A, K, Q, J, T, 9, 8, 7, 6, 5, 4, 3, or 2")
#       a bid number

# parse our input
hands_info = { }
for line_txt in data :
    hand_txt, bid_amt = line_txt.split(" ")
    hands_info[hand_txt] = int(bid_amt)

#print("Hands: " + str(hands_info) )

# calculate the sum of the money the hands win based on their rank and bids

CARD_ORDER = {
                "A":    14,
                "K":    13,
                "Q":    12,
                "J":    11,
                "T":    10,
                "9":    9,
                "8":    8,
                "7":    7,
                "6":    6,
                "5":    5,
                "4":    4,
                "3":    3,
                "2":    2,
             }

HAND_TYPES =    {
                    "FiveKind":     100,
                    "FourKind":      90,
                    "FullHouse":     75,
                    "ThreeKind":     50,
                    "TwoPair":       40,
                    "OnePair":       20,
                    "HighCard":       1,
                }

def get_hand_type (hand_txt,) :
    """
    Given a hand text, figure out what type it is.
    :param hand_txt:
    :return:
    """

    hand_counts = sorted(dict(Counter(hand_txt)).values())
    #print("Card counts in hand: " + str(hand_counts))
    high_count = hand_counts[-1]

    if high_count == 5 :
        return "FiveKind"
    elif high_count == 4 :
        return "FourKind"
    elif high_count == 3 :
        if hand_counts[-2] == 2 :
            return "FullHouse"
        else :
            return "ThreeKind"
    elif high_count == 2 :
        if hand_counts[-2] == 2 :
            return "TwoPair"
        else :
            return "OnePair"
    elif high_count == 1 :
        return "HighCard"

def compare_hands (hand1, hand2) :
    """
    Given two strings representing camel card hands, figure out who won or if they tied.

    :param hand1:
    :param hand2:
    :return: -1 if hand1 one is smaller, 1 if hand1 is bigger or 0 if the hands are equal
    """

    # the only case where the hands are equal is where they have exactly the same cards in the same order
    if hand1 == hand2 :
        print("Found equal hands: " + hand1 + " " + hand2) # this really shouldn't happen, because that would screw up our totals
        return 0

    hand1_type = get_hand_type(hand1)
    hand2_type = get_hand_type(hand2)

    # compare our types
    if   HAND_TYPES[hand1_type] > HAND_TYPES[hand2_type] :
        return 1
    elif HAND_TYPES[hand2_type] > HAND_TYPES[hand1_type] :
        return -1
    else :
        # they are the same type, so we need to compare the actual hands
        for c_idx in range(len(hand1)) :
            if   CARD_ORDER[hand1[c_idx]] > CARD_ORDER[hand2[c_idx]] :
                return 1
            elif CARD_ORDER[hand2[c_idx]] > CARD_ORDER[hand1[c_idx]] :
                return -1

    # hopefully there aren't any totally equal hands, but just in case
    print("Found equal hands: " + hand1 + " " + hand2)
    return 0

def calc_winnings (bid_amount, hand_idx, ) :
    """
    Given how much was bid by the holder and the index of a hand in the list of all hands
    calculate how much that hand won for the holder.
    :param bid_amount:
    :param hand_idx:
    :param hands_length:
    :return:
    """

    return bid_amount * (hand_idx + 1)

sorted_hands = sorted(hands_info.keys(), key=cmp_to_key(compare_hands),)
print("Sorted hands: " + str(sorted_hands))

total_wins = 0
for h_idx in range(len(sorted_hands)) :
    total_wins += calc_winnings(hands_info[sorted_hands[h_idx]], h_idx,)

print("Total winnings: " + str(total_wins))

"""
With my inputs: 253933213

Time for part 1: 3:03
(Note: there was a substantial break during part 1 that I didn't record.)
"""