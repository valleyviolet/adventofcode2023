#!/usr/bin/env python
# encoding: utf-8
"""
By Eva Schiffer
for the advent of code
"""

import re, sys
import numpy

#print("Args: " + str(sys.argv))
day_txt = "5"
file_to_open = "./input/day"+day_txt+"_input_test.txt" if len(sys.argv) > 1 and sys.argv[1] == "test" else "./input/day"+day_txt+"_input.txt"
file_obj = open(file_to_open)
data = file_obj.readlines()

NUMBERS_EXP = r'\d+'

# Parse input to find
#       the list of seed ranges
#           each pair is the start and length of a range of seed ids
#       a bunch of maps
#           seed-to-soil,
#           soil-to-fertilizer,
#           fertilizer-to-water,
#           water-to-light,
#           light-to-temperature,
#           temperature-to-humidity,
#           humidity-to-location

# all maps work the same way. They're lists of 3 numbers in the form:
#       destination range start, source range start, range length
# so the src and dest ranges are the same, this is only an offset
# you map numbers only if they're inside the source range

# loop through all lines
seed_list = None
maps = { }
line_idx = 0
#print("data: \n" + str(data))
line_idx = 0

# parse seeds
seed_list = numpy.array(re.findall(NUMBERS_EXP, data[line_idx],), dtype=numpy.int64,)
len_seeds = seed_list.size
seed_list = numpy.reshape(seed_list, (int(len_seeds/2), 2), )
line_idx += 2

# look through the rest of the file
while line_idx <= len(data) :

    # setup stuff
    map_name = data[line_idx].split(" ")[0]
    tmp_nums = None
    line_idx += 1

    # parse all lines in the map
    while line_idx < len(data) and len(data[line_idx]) > 1 :
        this_row = numpy.array(re.findall(NUMBERS_EXP, data[line_idx],), dtype=numpy.int64,)
        #print("tmp nums: " + str(tmp_nums))
        #print("this row: " + str(this_row))
        tmp_nums = [this_row,] if tmp_nums is None else numpy.append(tmp_nums, [this_row,], axis=0,)
        line_idx += 1

    maps[map_name] = tmp_nums

    line_idx += 1

#print("Seeds: " + str(seed_list) )
print("Seeds shape: " + str(seed_list.shape) + "\n")
#print("Maps: " + str(maps))

def apply_a_map (input_array, map_array, ) :
    """
    Given an input array and a map, figure out what the output is after mapping.
    Note: as of part 2, the input is sets of [start_value, range_size,] rather than concrete numbers.

    :param input_array: first dimension is which range, second is start_value or range_size
    :param map_array:
    :return:
    """
    output_array = None

    # map each input value separately
    for i in range(input_array.shape[0]) :
        this_start, this_range = input_array[i]
        this_end = this_start + this_range # for convenience
        this_out = None

        # check all lines in the map
        this_out_ranges = None
        for r_idx in range(map_array.shape[0]) :
            r_dst_start, r_src_start, r_size = map_array[r_idx]
            r_src_end = r_src_start + r_size # for convenience
            #print("Checking map line: " + str(map_array[r_idx]))

            # is it possible for our input to be on this line?
            if this_start < r_src_end and this_end >= r_src_start :

                # if our range is entirely within the translation rule
                if this_start >= r_src_start and this_end < r_src_end :
                    # we still have one range, just translate it's start point
                    this_out = numpy.array([[this_start - r_src_start + r_dst_start, this_range,],], dtype=numpy.int64)

                # otherwise, if our range overlaps the start of the rule but not the end
                elif this_start < r_src_start and this_end < r_src_end :
                    # we now will have two ranges,
                    #       the translated one and
                    #       the original part that was below the translation range
                    remains = numpy.array([[this_start, r_src_start - this_start,],], dtype=numpy.int64)
                    this_out = [[r_dst_start, this_range - (r_src_start - this_start),],]
                    this_out = numpy.append(this_out, apply_a_map(remains, map_array[r_idx+1:]), axis=0,)
                    break

                # our range overlaps the end of the rule but not the start
                elif this_start >= r_src_start and this_end >= r_src_end :
                    # we now will have two ranges,
                    #       the translated one and
                    #       the original part that was above the translation range
                    remains = numpy.array([[r_src_end, this_end - r_src_end,],], dtype=numpy.int64)
                    this_out = [[this_start-r_src_start+r_dst_start, r_src_end - this_start,],]
                    this_out = numpy.append(this_out, apply_a_map(remains, map_array[r_idx + 1:]), axis=0, )
                    break

                # our range overlaps both ends of the rule
                else :
                    # now we'll have three ranges,
                    #       the translated one
                    #       the original part that was below the translation range
                    #       the original part that was above the translation range
                    remains = numpy.array([[this_start, r_src_start - this_start,],
                                           [r_src_end,      this_end - r_src_end,],], dtype=numpy.int64)
                    this_out = [[r_dst_start, r_size,],]
                    this_out = numpy.append(this_out, apply_a_map(remains, map_array[r_idx + 1:]), axis=0, )
                    break

        # if we didn't find any matches, use the unaltered line
        this_out = this_out if this_out is not None else numpy.array([[this_start, this_range,],],
                                                                     dtype=input_array.dtype,)

        output_array = this_out if output_array is None else numpy.append(output_array, this_out, axis=0, )

    return output_array

apply_order = [ 'seed-to-soil',
                'soil-to-fertilizer',
                'fertilizer-to-water',
                'water-to-light',
                'light-to-temperature',
                'temperature-to-humidity',
                'humidity-to-location']

# Apply the various maps
results = { }
previous_results = seed_list
for map_name in apply_order :
    print("Applying map: " + map_name)
    new_results = apply_a_map(previous_results, maps[map_name],)
    results[map_name.split("-")[-1]] = new_results
    previous_results = new_results
    print("Results: " + str(previous_results))

print("Available after mapping: " + str(results.keys()))
#print("Seeds: " + str(seed_list))
#print("Soils: " + str(results["soil"]))
print("Locations: " + str(results["location"]))

# find the smallest location number
tmp_mask = results["location"][:,1] > 0
# (Note: there's still a bug in this where it allows length 0 ranges. I'm not sure which boundary check I did wrong)
min_location = numpy.min(results["location"][:,0][tmp_mask])

print("Smallest location number: " + str(min_location))

"""
With my inputs: 41222968

Time for part 1: 2:05
Time for part 2: 2:10
Total time for this day: 4:15
"""