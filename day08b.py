#!/usr/bin/env python
# encoding: utf-8
"""
By Eva Schiffer
for the advent of code
"""

import re, sys
import numpy

#print("Args: " + str(sys.argv))
day_txt = "8"
file_to_open = "./input/day"+day_txt+"_input_"+sys.argv[1]+".txt" if len(sys.argv) > 1 else "./input/day"+day_txt+"_input.txt"
file_obj = open(file_to_open)
data = file_obj.readlines()

#NUMBERS_EXP = r'\d+'
NODE_NAME_EXP = r'[A-Z,0-9]{3}'

# Parse input to find
#       a set of LR moves
#       a simple graph with two connections (left and right) per node

# parse our input
paths = { }
moves_str = data[0].strip()
line_idx = 2 # start on the third line
while line_idx < len(data) and len(data[line_idx]) > 2 :
    tmp = re.findall(NODE_NAME_EXP, data[line_idx],)
    paths[tmp[0]] = (tmp[1], tmp[2],)   # [0] in the tuple is the left path, [1] is the right path
    line_idx += 1

#print("Moves: " + moves_str )
print("Length of the moves: " + str(len(moves_str)) )
#print("Paths: " + str(paths))
#print("Nodes: " + str(paths.keys()))

def make_a_move (current_node, move_index, moves_str, paths_map,) :
    """
    Given where we are, where we are in the string of moves, the moves string, and the map of paths
    take one more move along the path.
    :param current_node:
    :param move_index:
    :param moves_str:
    :param paths_map:
    :return: The new current node and move index.
    """

    move_to_take = moves_str[move_index]
    new_move_idx = (move_index + 1) % len(moves_str)    # rollover when we get to the end of the moves string

    tmp_paths = paths_map[current_node]
    next_node = tmp_paths[0] if move_to_take == "L" else tmp_paths[1]

    return next_node, new_move_idx

def calc_move_idx (num_moves, moves_str,) :
    """
    Given a number of moves taken so far, what index in the moves_str should we be at?
    :param num_moves:
    :param moves_str:
    :return:
    """
    return num_moves % len(moves_str)

def get_moves_to_nextZ (current_node, move_index, moves_str, paths_map,) :
    """
    Given a starting node and move index, how many moves until we reach a node ending in "Z"?
    :param current_node:
    :param move_index:
    :param moves_str:
    :param paths_map:
    :return: a number of moves and the node we'll arrive at then (that node will end in "Z")
    """

    tmp_node = current_node
    tmp_mv_idx = move_index

    # take one move so we don't immediately hit the end if we started on a node ending in "Z"
    tmp_node, tmp_mv_idx = make_a_move(tmp_node, tmp_mv_idx, moves_str, paths, )
    num_moves = 1

    while tmp_node[-1] != "Z" :
        tmp_node, tmp_mv_idx = make_a_move(tmp_node, tmp_mv_idx, moves_str, paths, )
        num_moves += 1

    return num_moves, tmp_node

def find_next_step(start_node, start_at_moves_num, moves_str, paths, c_paths) :
    """
    Given a node we're starting at and when, check c_paths for the next "Z" node and if
    it isn't already there calculate and add it to c_paths; return the next node & num
    moves to that node information
    :param start_node:
    :param start_at_moves_num:
    :param moves_str:
    :param paths:
    :param c_paths:
    :return: a number of moves and the node we'll arrive at then (that node will end in "Z")
    """

    tmp_start_moves_idx = calc_move_idx(start_at_moves_num, moves_str,)
    #print("start node: " + str(start_node))
    #print("move idx:   " + str(tmp_start_moves_idx))
    if (start_node, tmp_start_moves_idx)  not in c_paths :
        tmp_num_moves, tmp_next_node = get_moves_to_nextZ(start_node, tmp_start_moves_idx, moves_str, paths, )
        c_paths[(start_node, tmp_start_moves_idx)] = (tmp_num_moves, tmp_next_node)

    return c_paths[(start_node, tmp_start_moves_idx)]

# figure out how many moves it takes to get from nodes that end with A to all nodes that end with Z?
num_moves = 0
move_idx = 0

# find our starting nodes
current_nodes = [n for n in paths.keys() if n[-1] == "A"]
moves_to_next = numpy.zeros(len(current_nodes), dtype=numpy.int64,)
print("Starting nodes: " + str(current_nodes))

# make a more complex version of our paths
# in this version the key is (node_name, move_index) and that maps to (number_of_moves, next_Z_ending_node)
# where you will arrive at next_Z_ending_node in number_of_moves following the movement rules if you start
# from node_name at move_index.
c_paths = { }
# calculate the complex paths for the starting nodes
for n_idx in range(len(current_nodes)) :
    tmp_node = current_nodes[n_idx]
    tmp_num_moves, tmp_next_node = find_next_step(tmp_node, 0, moves_str, paths, c_paths,)
    current_nodes[n_idx] = tmp_next_node
    moves_to_next[n_idx] = tmp_num_moves

tmp = 1
for x in moves_to_next :
    tmp *= x
print("Moves to next: " + str(moves_to_next))
tmp = numpy.array(moves_to_next, dtype=numpy.float64) / float(len(moves_str))
print("Moves to next / length of moves_str: " + str(tmp))
print("first Z nodes: " + str(current_nodes))

# run one round of calculations on the circut lengths
final_nodes = current_nodes
moves_to_final = numpy.copy(moves_to_next)
for n_idx in range(len(final_nodes)) :
    tmp_node = current_nodes[n_idx]
    tmp_num_moves, tmp_next_node = find_next_step(tmp_node, moves_to_next[n_idx], moves_str, paths, c_paths,)
    final_nodes[n_idx] = tmp_next_node
    moves_to_final[n_idx] = tmp_num_moves

print("final Z nodes: " + str(final_nodes))
pathln_div_mvlength = numpy.array(moves_to_final, dtype=numpy.float64) / float(len(moves_str))
print("Moves to final / length of moves_str: " + str(pathln_div_mvlength))

""" # this problem is delibrately built so that calculating the answer is impossible, so don't
# at this point every "current" node should be a Z ending node, but we aren't actually at any of them.
while moves_to_next.sum() > 0 :
    min_moves_to_change = numpy.min(moves_to_next)
    moves_to_next -= min_moves_to_change
    num_moves += min_moves_to_change

    # if someone still has moving to do, take the next steps
    if moves_to_next.sum() > 0 :
        for n_idx in range(len(current_nodes)) :
            if moves_to_next[n_idx] <= 0 :
                tmp_node = current_nodes[n_idx]
                tmp_num_moves, tmp_next_node = find_next_step(tmp_node, num_moves, moves_str, paths, c_paths, )
                current_nodes[n_idx] = tmp_next_node
                moves_to_next[n_idx] = tmp_num_moves
"""

print("Complex paths: " + str(c_paths))
#print("Moves needed: " + str(num_moves))

"""
Things I learned by inspecting the paths and lengths:
    1)  There is only one node ending in Z you can get to for a starting node 
        and that Z node can only go to itself (not any other Z node).
    2)  The paths from the start nodes to the end nodes are the same length as 
        the paths from the Z nodes to themselves.
    3)  All the paths we care about are evenly divisible in length by the move 
        string (so you'll always start the path at the beginning of the move
        string). 
        
    To be honest I think this is shitty puzzle design. I much prefer puzzles 
    whose solutions flow from the puzzle descriptions, rather than requiring 
    deep analysis of the undocumented input data. :(
    
    in my input data the moves length is 281 and the path lengths are:
        [53. 73. 79. 71. 61. 43.] times that
"""

total_mult = 1
for x in pathln_div_mvlength :
    total_mult *= x
total_mult *= len(moves_str)
print("Total multiplication: " + str(total_mult)) # this gave me the answer the AoC wanted
# Note: I don't think this is a guaranteed method to solve the problem if there are other
# shared factors besides the move length, but I'm not solving this any further because I'm
# pretty mad about the puzzle design.

"""
With my inputs: 

Time for part 1: 0:25 
Time for part 2: 2:12
Total time for this day: 2:37
(A good deal of my part 2 time on this was me being frusrtated and avoiding working on it.)
"""