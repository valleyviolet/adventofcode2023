#!/usr/bin/env python
# encoding: utf-8
"""
By Eva Schiffer
for the advent of code
"""

import numpy, re

file_obj = open("./input/day1_input.txt")
data = file_obj.readlines()

word_digit_table =  {
                        "one":   "one1one",
                        "two":   "two2two",
                        "three": "three3three",
                        "four":  "four4four",
                        "five":  "five5five",
                        "six":   "six6six",
                        "seven": "seven7seven",
                        "eight": "eight8eight",
                        "nine":  "nine9nine",
                    }

def replace_words (in_str,) :
    """
    Given a code line, replace any words that appear from word_digit_table with their digits.
    :param in_str:
    :return:
    """
    out_str = in_str

    for key in word_digit_table :
        value = word_digit_table[key]
        out_str = out_str.replace(key, value,)

    return out_str


code_list = numpy.array([], dtype=int,)
for line_txt in data :

    print("original line: " + line_txt)
    tmp_line = replace_words(line_txt,)
    print("modified line: " + tmp_line)
    temp = ''.join(re.findall(r'\d+', tmp_line))
    print("all digits: " + temp)
    #print("start: " + temp[0])
    this_code = int(temp[0] + temp[-1])
    print("final code: " + str(this_code))
    code_list = numpy.append(code_list, this_code,)

cal_sum = numpy.sum(code_list)

print("code list: " + str(code_list))
print("total: " + str(cal_sum))

"""
Total time for both parts = just under an hour
"""