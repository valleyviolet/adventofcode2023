#!/usr/bin/env python
# encoding: utf-8
"""
By Eva Schiffer
for the advent of code
"""

import re, sys
import numpy

#print("Args: " + str(sys.argv))
day_txt = "6"
file_to_open = "./input/day"+day_txt+"_input_"+sys.argv[1]+".txt" if len(sys.argv) > 1 else "./input/day"+day_txt+"_input.txt"
file_obj = open(file_to_open)
data = file_obj.readlines()

NUMBERS_EXP = r'\d+'

MAX_VAL = numpy.iinfo(numpy.int64).max # there are other ways to handle this, but this should be ok

# Parse input to find
#       a time
#       a distance
# oops bad kerning, just one number each!

# times are in ms; distances are in mm

# parse our input
times_list = [''.join(re.findall(NUMBERS_EXP, data[0],), ),]
dists_list = [''.join(re.findall(NUMBERS_EXP, data[1],), ),]

print("Times:     " + str(times_list) )
print("Distances: " + str(dists_list) )

# The equation that describes our movement is:
#       distance = time_held * (total_time - time_held)
#
# where the total time for a race is known and how long
# the button is held is our choice. The minimum distance
# to beat is also known.
# there are going to be multiple solutions where our
# distance is > the minimum distance.

def solve_quadratic(a_val, b_val, c_val,) :
    """
    Given concrete a, b, and c, return the two possible solutions to the quadratic equation.
    We're using the quadratic equation:
            x = ( -b ± sqrt( b^2 - 4ac) ) / 2a

    relearning the quadratic equation:
    https://www.khanacademy.org/test-prep/sat/x0a8c2e5f:untitled-652/x0a8c2e5f:passport-to-advanced-math-lessons-by-skill/a/gtp--sat-math--article--solving-quadratic-equations--lesson

    :param a_val:
    :param b_val:
    :param c_val:
    :return: The two solutions in ascending order.
    """
    tmp_a = float(a_val)
    tmp_b = float(b_val)
    tmp_c = float(c_val)

    tmp_root = numpy.sqrt((tmp_b * tmp_b) - (4 * tmp_a * tmp_c) )
    x1 = ((-1 * tmp_b) + tmp_root) / (2 * tmp_a)
    x2 = ((-1 * tmp_b) - tmp_root) / (2 * tmp_a)

    return min(x1, x2,), max(x1, x2,)

# calculate the possible holding times/speeds
ways_to_win = [ ]
total_win = 1
for race_num in range(len(times_list)) :
    r_time     = int(times_list[race_num])
    r_min_dist = int(dists_list[race_num])
    print("race time: " + str(r_time))

    # to use the quadratic formula, a = -1, b = total_time, c = - total_distance
    s1, s2 = solve_quadratic(-1, r_time, -1 * r_min_dist,)

    print ("Solutions for distance " + str(r_min_dist) + " and time " + str(r_time) + ": " + str(s1) + ", " + str(s2))

    to_win_this = int(numpy.floor(s2) - numpy.ceil(s1) + 1)

    ways_to_win.append(to_win_this,)
    total_win *= to_win_this

print("Number of ways to win: " + str(ways_to_win))
print("Multiplication of ways to win: " + str(total_win))

"""
With my inputs: 35349468

(I didn't time this extra version of part 2.)
"""