#!/usr/bin/env python
# encoding: utf-8
"""
By Eva Schiffer
for the advent of code
"""

import re, sys, os
import numpy

NUMBERS_EXP = r'-?\d+' # ? is zero or 1 times, so numbers can now be negative

#print("Args: " + str(sys.argv))
day_txt = re.findall(NUMBERS_EXP, os.path.basename(vars(sys.modules[__name__])['__file__']))[0]
file_to_open = "./input/day"+day_txt+"_input_"+sys.argv[1]+".txt" if len(sys.argv) > 1 else "./input/day"+day_txt+"_input.txt"
file_obj = open(file_to_open)
data = file_obj.readlines()

# Parse input to find
#       a map of the city on Gear Island

"""
The numbers on the map represent how much heat our crucible will lose when we enter that tile. 
The starting point is the upper left (0,0). 
The goal destination (the factory) is in the bottom right (max_line, max_col). 

The crucible movement:
    The crucible has a direction it's moving. 
    The crucible can't move in the same direction for more than 3 squares at a time.
    The crucible can choose to go straight, turn 90 degrees right, or turn 90 degrees left.

We want to get from the start to the factory, with a minimum of heat loss. 

Note, I think this is probably a textbook case for an A* search. 

Key information required for each "in-process" state
    * where is the crucible
    * how much heat have we lost already (a measure of how good our path is so far)
    * how far to the exit (our heuristic for whether we're making progress to the goal)

"""

# parse our input
heat_loss_map = numpy.zeros((len(data), len(data[0].strip())), dtype=numpy.int16,)
# step through all the lines
line_idx = 0
while line_idx < len(data) :
    heat_loss_map[line_idx,:] = list(data[line_idx].strip())
    line_idx += 1

NORTH = "north"
SOUTH = "south"
EAST  = "east"
WEST  = "west"
TO_GO = {
            NORTH:  (-1,  0),
            SOUTH:  ( 1,  0),
            WEST:   ( 0, -1),
            EAST:   ( 0,  1),
        }
CAN_GO =    {
                NORTH:  {NORTH, EAST, WEST},
                SOUTH:  {SOUTH, EAST, WEST},
                WEST:   {WEST, NORTH, SOUTH},
                EAST:   {EAST, NORTH, SOUTH},
            }

def print_map(map_array, indent=0, true_char='#', false_char='.', extra_stuff_positions=None, ) :
    """
    Given a map info that is a string or bool, print it in a user readable way.
    :param map_array:
    :param indent:
    :param true_char:
    :param false_char:
    :param extra_stuff_positions:
    :return:
    """
    extra_stuff_positions = set( ) if extra_stuff_positions is None else extra_stuff_positions

    indent_txt = ""
    for i in range(indent) :
        indent_txt += "\t"

    for l_idx in range(map_array.shape[0]) :
        this_line = indent_txt
        for c_idx in range(map_array.shape[1]) :
            if map_array.dtype == bool :
                tmp_char = true_char if map_array[l_idx,c_idx] else false_char
                                                                                        # invert extra stuff
                tmp_char = tmp_char if (l_idx,c_idx) not in extra_stuff_positions else "\033[7m" + tmp_char + "\033[0m"
                this_line += tmp_char
            else :
                tmp_char = str(map_array[l_idx,c_idx])
                                                                                        # invert extra stuff
                tmp_char = tmp_char if (l_idx, c_idx) not in extra_stuff_positions else "\033[7m" + tmp_char + "\033[0m"
                this_line += tmp_char
        print(this_line)

def is_in_map (l_idx, c_idx, map_shape) :
    """
    Is the given position inside the map?
    :param l_idx:
    :param c_idx:
    :param map_shape:
    :return:
    """
    return (l_idx >= 0)  and (l_idx < map_shape[0]) and (c_idx >= 0) and (c_idx < map_shape[1])

# visualize input to make sure we parsed it correctly
do_print = True
if do_print :
    print("Heat loss map: ")
    print_map(heat_loss_map, indent=1,)

"""
The crucible movement:
    The crucible has it's total heat loss so far.
    The crucible has a direction it's moving. 
    The crucible can't move in the same direction for more than 3 squares at a time.
    The crucible can choose to go straight, turn 90 degrees right, or turn 90 degrees left.
"""
class CrucibleState :

    def __init__(self, heat_loss, position, direction, previous_path, goal_line, goal_column, visited_set,) :
        """
        Generate a new crucible state with the direction it'll be going and some other information about it's
        past and goal.

        :param heat_loss:
        :param direction:
        :param position:
        :param previous_path:
        :param goal_line:
        :param goal_column:
        """

        self.heat_loss = heat_loss
        self.dir = direction
        self.position = position
        self.path = previous_path.copy()
        self.goal = (goal_line, goal_column)
        self.visited = visited_set.copy()
        self.t_cost = self.heat_loss + (self.goal[0] - self.position[0]) + (self.goal[1] - self.position[1])

    def get_next_moves(self, heat_map, ) :
        """
        Given the map of the heat loss in the area, return the possible next places we could go from here.
        :param heat_map:
        :return:
        """
        possible_dirs = CAN_GO[self.dir].copy()
        # if we've already gone the same way three times, we can't go that way anymore
        #print("my path: " + str(self.path))
        if len(self.path) > 2 and (self.path[-1] == self.dir and self.path[-2] == self.dir and self.path[-3] == self.dir) :
            possible_dirs.remove(self.dir,)

        # make our resulting states
        to_return = [ ]
        for dir in possible_dirs :
            tmp_new_position = (self.position[0] + TO_GO[dir][0], self.position[1] + TO_GO[dir][1])
            if is_in_map(tmp_new_position[0], tmp_new_position[1], heat_map.shape,) :
                tmp_new_heatloss = self.heat_loss + heat_map[tmp_new_position]
                tmp_new_path = self.path.copy()
                tmp_new_path.append(dir)
                tmp_new_visited = self.visited.copy()
                tmp_new_visited.add((tmp_new_position),)
                to_return.append(CrucibleState(tmp_new_heatloss, tmp_new_position, dir, tmp_new_path,
                                               self.goal[0], self.goal[1], tmp_new_visited,))

        return to_return

    def num_previous_dir_moves(self,) :
        """
        How many moves have we made in the same direction?
        :return:
        """
        tmp_len_path = len(self.path)
        if tmp_len_path == 0 :
            return 0
        if tmp_len_path == 1 :
            return 1
        if tmp_len_path == 2 :
            if self.path[-2] != self.path[-1] :
                return 1
            else :
                return 2
        # at this point tmp_len_path must be >= 3
        if self.path[-2] == self.path[-1] :
            if self.path[-3] == self.path[-2] :
                return 3
            else :
                return 2
        else :
            return 1

    def total_cost (self, ) :
        """
        What is the cost of taking this path?
        :return:
        """
        return self.t_cost

    def at_goal (self, ) :
        return self.position[0] == self.goal[0] and self.position[1] == self.goal[1]

    def __gt__(self, other) :
        """Is this greater than the other?
        """
        return self.t_cost > other.t_cost

    def __str__ (self) :
        return "CrusibleState " + str(self.position) + " " + str(self.dir) + " (cost " + str(self.total_cost()) + ")"

from queue import PriorityQueue

# Run an A* search to try to find the  best path
goal_line = heat_loss_map.shape[0] - 1
goal_col  = heat_loss_map.shape[1] - 1
tmp_start1 = CrucibleState(0, (0, 0), SOUTH, [], goal_line, goal_col, {(0,0),},)
tmp_start2 = CrucibleState(0, (0, 0), EAST,  [], goal_line, goal_col, {(0,0),},)
win = None
TODO_states = PriorityQueue()
TODO_states.put(tmp_start1)
TODO_states.put(tmp_start2)
best_cache = { } # will be indexed with (l_idx, c_idx, direction, num_times_that_direction)
best_cache[(0,0, SOUTH,0)] = tmp_start1
best_cache[(0,0, EAST, 0)] = tmp_start2
# this is a bad way to loop, but whatever
while not TODO_states.empty() :
    #print("TODO states: " + str(TODO_states))
    curr_best = TODO_states.get( )
    #print("next best state todo: " + str(curr_best))
    #print_map(heat_loss_map, indent=1, extra_stuff_positions=curr_best.visited, )
    if curr_best.at_goal( ) :
        win = curr_best
        break
    # otherwise, go on with making the next steps
    next_nodes = curr_best.get_next_moves(heat_loss_map)
    #print("Found possible future nodes: " + str(next_nodes))
    for tmp_node in next_nodes :
        this_cost = tmp_node.total_cost( )
        if (tmp_node.position[0], tmp_node.position[1], tmp_node.dir, tmp_node.num_previous_dir_moves(),) in best_cache :
            if best_cache[(tmp_node.position[0], tmp_node.position[1],
                           tmp_node.dir, tmp_node.num_previous_dir_moves(),)].total_cost() > tmp_node.total_cost() :
                best_cache[(tmp_node.position[0], tmp_node.position[1], tmp_node.dir, tmp_node.num_previous_dir_moves(),)] = tmp_node
                TODO_states.put(tmp_node)
        else :
            best_cache[(tmp_node.position[0], tmp_node.position[1], tmp_node.dir, tmp_node.num_previous_dir_moves(),)] = tmp_node
            TODO_states.put(tmp_node)

print("Resulting path: ")
print_map(heat_loss_map, indent=1, extra_stuff_positions=win.visited,)

print("best heat loss at goal: " + str(win.heat_loss))

"""
With my inputs: 1008

Time for part 1: (I lost track over several days.)
"""