#!/usr/bin/env python
# encoding: utf-8
"""
By Eva Schiffer
for the advent of code
"""

import numpy, re

file_obj = open("./input/day3_input.txt")
data = file_obj.readlines()

GEARS_EXP   = r"\*"
NUMBERS_EXP = r'\d+'

def find_all_with_positions (expression, to_look_in, exclude=None,) :
    """
    Given an expression and a string to search, find the positions and matches that aren't in the exclude list.
    :param expression:
    :param to_look_in:
    :param exclude:
    :return:
    """

    if exclude is None :
        exclude = set()

    found = set()

    for foundThing in re.finditer(expression, to_look_in) :
        match_txt = foundThing.group()
        if match_txt not in exclude :
            found.add((foundThing.start(), match_txt))

    return found

def generate_surrounding_idxes (line_idx, col_idx, ) :
    """
    Given a line and column index, return the tuples of all the surrounding indexes,
    including the input index and any directly or diagonally adjacent.

    :param line_idx:
    :param col_idx:
    :return:
    """

    to_return = set( )
    for l_idx in range(line_idx-1, line_idx+2,) :
        for c_idx in range(col_idx-1, col_idx+2,) :
            to_return.add ((l_idx, c_idx,))

    return to_return

# Parse input to find
#       all numbers and their starting positions
#       the positions of any "symbols" and the spaces around them

line_idx = 0
all_nums = set()
potential_gears = set()

# loop through all lines
for line_txt in data :

    # grab any numbers
    tmp_nums = find_all_with_positions(NUMBERS_EXP, line_txt, )
    for num_info in tmp_nums :
        col_idx, num_txt = num_info
        all_nums.add((line_idx, col_idx, num_txt,))

    # grab all potential gears
    tmp_sym = find_all_with_positions(GEARS_EXP, line_txt, )
    for sym_info in tmp_sym :
        col_idx, sym_txt = sym_info
        print("Found gear at : " + str(line_idx) + ", " + str(col_idx))
        potential_gears.add((line_idx, col_idx,))

    line_idx += 1

# make a map of where our numbers are
number_map = { }
for f_l_idx, f_c_idx, f_num in sorted(all_nums) :

    # find the spaces inside the number
    for c_idx in range(f_c_idx, f_c_idx + len(f_num), ):
        number_map[(f_l_idx, c_idx,)] = (f_l_idx, f_c_idx, f_num,) # note: the indexes plus the number text is our unique index for a number

print("Found numbers: " + str(all_nums) + "\n")
print("Found gears:   " + str(potential_gears) + "\n")

def get_num_around_gear (gear_line_idx, gear_col_idx, number_map, ) :
    """
    Given the position of a gear, figure out what numbers are adjacent to it and return them.
    :param gear_line_idx:
    :param gear_col_idx:
    :param number_map:
    :return:
    """

    found_nums = set()
    near_gear = generate_surrounding_idxes(gear_line_idx, gear_col_idx)

    for l_idx, c_idx in near_gear :
        if (l_idx, c_idx,) in number_map :
            found_nums.add(number_map[(l_idx, c_idx,)])

    return found_nums

# For each potential gear we found, check to see if it's adjacent to two numbers
# and add the gear ratio to our sum if it is.
total_vals = 0
for line_idx, col_idx in potential_gears :

    attached_nums = get_num_around_gear(line_idx, col_idx, number_map,)

    if len(attached_nums) == 2 :
        gear_ratio = 1
        for l_idx, c_idx, num_txt in attached_nums :
            gear_ratio *= int(num_txt)
        total_vals += gear_ratio

print("Total sum gear ratios: " + str(total_vals))

"""
With my inputs: 72514855

Time for part 1: 1:55
Time for part 2: 0:27
Total time for this day: 2:22
"""