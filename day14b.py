#!/usr/bin/env python
# encoding: utf-8
"""
By Eva Schiffer
for the advent of code
"""

import re, sys, os
import numpy

NUMBERS_EXP = r'-?\d+' # ? is zero or 1 times, so numbers can now be negative

#print("Args: " + str(sys.argv))
day_txt = re.findall(NUMBERS_EXP, os.path.basename(vars(sys.modules[__name__])['__file__']))[0]
file_to_open = "./input/day"+day_txt+"_input_"+sys.argv[1]+".txt" if len(sys.argv) > 1 else "./input/day"+day_txt+"_input.txt"
file_obj = open(file_to_open)
data = file_obj.readlines()

# Parse input to find
#       a map of the platform with round and square rocks

"""
    '#' is a square rock that doesn't move
    'O' is a round rock that can roll
    '.' is an empty space

North is the direction toward line number decreasing.
(I think this is consistent with the directions in previous maps.)
"""

NORTH = "north"
SOUTH = "south"
EAST  = "east"
WEST  = "west"
TO_GO = {
            NORTH:  (-1,  0),
            SOUTH:  ( 1,  0),
            WEST:   ( 0, -1),
            EAST:   ( 0,  1),
        }

def find_all_with_positions (expression, to_look_in, ) :
    """
    Given an expression and a string to search, find the positions and matches.
    :param expression:
    :param to_look_in:
    :return:
    """

    found = set()
    for foundThing in re.finditer(expression, to_look_in) :
        match_txt = foundThing.group()
        found.add((foundThing.start(), match_txt))

    return found

# parse our input
platform_shape = (len(data), len(data[0].strip()))
is_square_rocks = numpy.zeros(platform_shape, dtype=bool,)
round_rock_positions = set( )
for line_idx in range(len(data)) :
    #print("line: " + data[line_idx].strip())
    this_ln_map = numpy.array(list(data[line_idx].strip()), dtype=numpy.str_, ) == '#'
    is_square_rocks[line_idx,:] = this_ln_map
    tmp_round_positions = find_all_with_positions(r'O', data[line_idx].strip())
    for tmp_info in tmp_round_positions :
        round_rock_positions.add((line_idx, tmp_info[0]))
    #print("map of square rocks on this line: " + str(this_ln_map))
    #print("round rocks at: " + str(tmp_round_positions))

def print_map(map_array, indent=0, true_char='#', false_char='.', extra_stuff_positions=None, extra_stuff_char='O', ) :
    """
    Given a map info that is a string or bool, print it in a user readable way.
    :param map_array:
    :return:
    """
    extra_stuff_positions = set( ) if extra_stuff_positions is None else extra_stuff_positions

    indent_txt = ""
    for i in range(indent) :
        indent_txt += "\t"

    for l_idx in range(map_array.shape[0]) :
        this_line = indent_txt
        for c_idx in range(map_array.shape[1]) :
            if map_array.dtype == bool :
                tmp_char = true_char if map_array[l_idx,c_idx] else false_char
                tmp_char = tmp_char if (l_idx,c_idx) not in extra_stuff_positions else extra_stuff_char
                this_line += tmp_char
            else :
                tmp_char = str(map_array[l_idx,c_idx])
                tmp_char = tmp_char if (l_idx, c_idx) not in extra_stuff_positions else extra_stuff_char
                this_line += tmp_char
        print(this_line)

# visualize input to make sure we parsed it correctly
disp_map = False
if disp_map :
    print("\nOverall input map after parsing: ")
    print_map(is_square_rocks, indent=1, extra_stuff_positions=round_rock_positions,)
    print()

def tilt_north (sq_rock_map, rd_rock_pos, ) :
    """
    Given a map of the square rocks and the positions of the round rocks, tilt the platform
    so all the rocks fall to the north.
    :param sq_rock_map:
    :param rd_rock_pos:
    :return:
    """
    new_positions = rd_rock_pos

    # loop over all the rocks, sorted by line (so the northmost will be first)
    for rock_position in sorted(rd_rock_pos) :
        tmp_new_pos = roll_rock_north(rock_position, sq_rock_map, new_positions,)
        new_positions.remove(rock_position)
        new_positions.add(tmp_new_pos)

    return new_positions

def roll_rock_north(rock_position, sq_rock_map, all_rd_rock_positions,) :
    """
    Given a single round rock, roll it as far north as it will go
    (Note: does not modify the input parameters)
    :param rock_position:
    :param sq_rock_map:
    :param rd_rock_positions:
    :return: the final position of the rock that started at rock_position
    """

    tmp_position = rock_position
    r_col_idx = rock_position[1]

    for line_idx in range(rock_position[0]-1, 0-1, -1) :
        if (not sq_rock_map[line_idx, r_col_idx]) and (line_idx,r_col_idx) not in all_rd_rock_positions :
            tmp_position = (line_idx,r_col_idx)
        else :
            break # this is ugly but I don't care

    return tmp_position

def score_map(platform_shape, rd_rock_positions, ) :
    """
    Given the platform shape and where the rocks are, calculate the load on the north side of the platform.
    :param platform_shape:
    :param rd_rock_positions:
    :return:
    """

    total_score = 0
    num_lines = platform_shape[0]

    for rock_position in rd_rock_positions :
        r_l_idx, r_c_idx = rock_position
        total_score += num_lines - r_l_idx

    return total_score

def transpose_coords(coords,) :
    """
    Given a (l,c) coordinate set, transpose it.
    :return:
    """
    return (coords[1], coords[0])

def flip_coords(coords, l_size,) :
    """
    Given a (l,c) coordinate set, flip it along l
    :param coords:
    :return:
    """
    return ((l_size - 1) - coords[0], coords[1])

def coords_from_north(coords_set, new_direction, area_shape, ):
    """
    Given a coordinate set and the direction you want to turn it to,
    translate to a new set that is shifted so north is new_direction
    :param new_direction:
    :return:
    """
    # probably won't happen, might as well check
    if new_direction == NORTH :
        return coords_set
    if new_direction == SOUTH :
        tmp = set( )
        for tmp_coord in coords_set :
            tmp.add(flip_coords(tmp_coord, area_shape[0],))
        return tmp
    if new_direction == WEST :
        tmp = set( )
        for tmp_coord in coords_set :
            tmp.add(transpose_coords(tmp_coord,))
        return tmp
    if new_direction == EAST :
        tmp = set ( )
        for tmp_coord in coords_set :
            tmp.add(flip_coords(transpose_coords(tmp_coord,), area_shape[1],))
        return tmp

def coords_to_north(coords_set, from_direction, area_shape, ) :
    """
    Given a coordinate set and the direction you want to turn it back from,
    translate to a new set that is shifted back to real north
    :param from_direction:
    :return:
    """
    # for everything but west the transform is the same in reverse
    if from_direction != EAST :
        return coords_from_north(coords_set, from_direction, area_shape, )
    else :
        tmp = set()
        for tmp_coord in coords_set:
            tmp.add(transpose_coords(flip_coords(tmp_coord, area_shape[0] )))
        return tmp

SPIN_ORDER = [NORTH, WEST, SOUTH, EAST,]
is_square_rocks_all =   {
                            NORTH:  is_square_rocks,
                            SOUTH:  numpy.flip(is_square_rocks, axis=0,),
                            EAST:   numpy.flip(numpy.transpose(is_square_rocks), axis=0,),
                            WEST:   numpy.transpose(is_square_rocks),
                        }

def do_a_spin (sq_rocks_all_dirs, rd_rocks_pts) :
    """
    Given some rock info, do a spin (four tilts) and return the new round rock positions
    :param sq_rocks_mask:
    :param rd_rocks_pts:
    :return:
    """
    new_positions = rd_rocks_pts

    #print_map(sq_rocks_all_dirs[NORTH], indent=1, extra_stuff_positions=new_positions,)

    starting_shape = sq_rocks_all_dirs[NORTH].shape
    for dir in SPIN_ORDER :
        #print("Tilting " + dir)
        tmp_sq_rocks = sq_rocks_all_dirs[dir]
        tmp_rd_rocks = coords_from_north(new_positions, dir, starting_shape,)
        new_positions = tilt_north(tmp_sq_rocks, tmp_rd_rocks, )
        new_positions = coords_to_north(new_positions, dir, tmp_sq_rocks.shape,)
        #print("After tilt: ")
        #print_map(sq_rocks_all_dirs[NORTH], indent=1, extra_stuff_positions=new_positions, )

    return new_positions

# spin our platform
print("Spinning platform...")
results_cache = { }
new_round_rock_positions = round_rock_positions
cycle_info = None
for count in range(1, 1000) :
    old_rock_positions = new_round_rock_positions.copy()
    new_round_rock_positions = do_a_spin(is_square_rocks_all, new_round_rock_positions,)

    print("Total load of rocks at count (" + str(count) + "): " + str(
        score_map(platform_shape, new_round_rock_positions, )))

    tmp_str = str(new_round_rock_positions)
    if tmp_str in results_cache :
        print("Found repeating pattern after spin number: " + str(count))
        print("This state previously existed after spin number: " + str(results_cache[tmp_str]))
        cycle_info = (results_cache[tmp_str], count)
        break
    results_cache[str(new_round_rock_positions)] = count

    # double check that our rocks moved as expected
    disp_map = False
    if disp_map :
        print("\nRocks after tipping: ")
        print_map(is_square_rocks, indent=1, extra_stuff_positions=new_round_rock_positions,)
        print()

desired_total_spins = 1000000000
cycle_length = cycle_info[1] - cycle_info[0]
final_idx = (desired_total_spins - cycle_info[0]) % cycle_length
print("Expected output on step " + str(final_idx) + " of cycle (" + str(cycle_info[0] + final_idx) + " spins in).")
# I could actually figure out the number in code, but instead I just read my output from the above totals and picked it

"""
With my inputs: 88371

Time for part 1: 1:04
Time for part 2: 2:07
Total time for this day: 3:11
"""