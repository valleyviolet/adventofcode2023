#!/usr/bin/env python
# encoding: utf-8
"""
By Eva Schiffer
for the advent of code
"""

import re, sys
import numpy

#print("Args: " + str(sys.argv))
day_txt = "5"
file_to_open = "./input/day"+day_txt+"_input_test.txt" if len(sys.argv) > 1 and sys.argv[1] == "test" else "./input/day"+day_txt+"_input.txt"
file_obj = open(file_to_open)
data = file_obj.readlines()

NUMBERS_EXP = r'\d+'

# Parse input to find
#       the list of seeds
#       a bunch of maps
#           seed-to-soil,
#           soil-to-fertilizer,
#           fertilizer-to-water,
#           water-to-light,
#           light-to-temperature,
#           temperature-to-humidity,
#           humidity-to-location

# all maps work the same way. They're lists of 3 numbers in the form:
#       destination range start, source range start, range length
# so the src and dest ranges are the same, this is only an offset
# you map numbers only if they're inside the source range

# loop through all lines
seed_list = None
maps = { }
line_idx = 0
#print("data: \n" + str(data))
line_idx = 0

# parse seeds
seed_list = numpy.array(re.findall(NUMBERS_EXP, data[line_idx],), dtype=numpy.int64,)
line_idx += 2

# look through the rest of the file
while line_idx <= len(data) :

    # setup stuff
    map_name = data[line_idx].split(" ")[0]
    tmp_nums = None
    line_idx += 1

    # parse all lines in the map
    while line_idx < len(data) and len(data[line_idx]) > 1 :
        this_row = numpy.array(re.findall(NUMBERS_EXP, data[line_idx],), dtype=numpy.int64,)
        #print("tmp nums: " + str(tmp_nums))
        #print("this row: " + str(this_row))
        tmp_nums = [this_row,] if tmp_nums is None else numpy.append(tmp_nums, [this_row,], axis=0,)
        line_idx += 1

    maps[map_name] = tmp_nums

    line_idx += 1

#print("Seeds: " + str(seed_list) + "\n")
#print("Maps: " + str(maps))

def apply_a_map (input_array, map_array, ) :
    """
    Given an input array and a map, figure out what the output is after mapping.

    :param input_array:
    :param map_array:
    :return:
    """
    output_array = numpy.copy(input_array,)

    # map each input value separately
    for i in range(input_array.size) :
        this_in = input_array[i]
        #print("Input value: " + str(this_in))

        # check all lines in the map
        out_tmp = this_in
        for r_idx in range(map_array.shape[0]) :
            #print("Checking map line: " + str(map_array[r_idx]))
            tmp = this_in - map_array[r_idx, 1]
            if tmp >= 0 and tmp < map_array[r_idx, 2] :
                #print("Line matched!")
                out_tmp = tmp + map_array[r_idx, 0]

        output_array[i] = out_tmp

    return output_array

apply_order = [ 'seed-to-soil',
                'soil-to-fertilizer',
                'fertilizer-to-water',
                'water-to-light',
                'light-to-temperature',
                'temperature-to-humidity',
                'humidity-to-location']

# Apply the various maps
results = { }
previous_results = seed_list
for map_name in apply_order :
    print("Applying map: " + map_name)
    new_results = apply_a_map(previous_results, maps[map_name],)
    results[map_name.split("-")[-1]] = new_results
    previous_results = new_results

print("Available after mapping: " + str(results.keys()))
#print("Seeds: " + str(seed_list))
#print("Soils: " + str(results["soil"]))
print("Locations: " + str(results["location"]))

# find the smallest location number
min_location = numpy.min(results["location"])

print("Smallest location number: " + str(min_location))

"""
With my inputs: 457535844

Time for part 1: 2:05
"""