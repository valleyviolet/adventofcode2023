#!/usr/bin/env python
# encoding: utf-8
"""
By Eva Schiffer
for the advent of code
"""

import re, sys, os

import numpy

NUMBERS_EXP = r'-?\d+' # ? is zero or 1 times, so numbers can now be negative

#print("Args: " + str(sys.argv))
day_txt = re.findall(NUMBERS_EXP, os.path.basename(vars(sys.modules[__name__])['__file__']))[0]
file_to_open = "./input/day"+day_txt+"_input_"+sys.argv[1]+".txt" if len(sys.argv) > 1 else "./input/day"+day_txt+"_input.txt"
file_obj = open(file_to_open)
data = file_obj.readlines()

# Parse input to find
#       a set of maps of ash and rocks (separated by blank lines)

# '.' is ash
# '#' is rocks
# the maps appear to be individually rectangular, but each map can be a different size

# parse our input
line_idx = 0
all_maps = { }
while line_idx < len(data) :
    tmp_array = numpy.array([], dtype=bool,)
    shape_cols = len(data[line_idx].strip())
    shape_lns  = 0
    tmp_first_ln = line_idx
    # read one map
    while line_idx < len(data) and len(data[line_idx].strip()) > 0 :
        #print("line: " + data[line_idx].strip())
        this_ln_as_arr = numpy.array(list(data[line_idx].strip()), dtype=numpy.str_,) == '#'
        #print("tmp array shape: " + str(tmp_array.shape))
        #print("this line shape: " + str(this_ln_as_arr.shape))
        tmp_array = numpy.append(tmp_array, this_ln_as_arr,)
        line_idx += 1
        shape_lns += 1
    # integerate this map data with overall data
    all_maps[tmp_first_ln] = tmp_array.reshape((shape_lns, shape_cols),)
    line_idx += 1

# Note: my representation is a boolean map of where the rocks are in the array

def print_map(map_array, indent=0, true_char='#', false_char='.', ) :
    """
    Given a map info that is a string or bool, print it in a user readable way.
    :param map_array:
    :return:
    """

    indent_txt = ""
    for i in range(indent) :
        indent_txt += "\t"

    for l_idx in range(map_array.shape[0]) :
        this_line = indent_txt
        for c_idx in range(map_array.shape[1]) :
            if map_array.dtype == bool :
                this_line += true_char if map_array[l_idx,c_idx] else false_char
            else :
                this_line += str(map_array[l_idx,c_idx])
        print(this_line)

# visualize input to make sure we parsed it correctly
print("Parsed " + str(len(all_maps.keys())) + " maps.")
print()
do_print_maps = False
if do_print_maps :
    for tmp_key in sorted(all_maps.keys()) :
        print_map(all_maps[tmp_key], indent=1,)
        print()

def find_equal_adjacent_lines (map, ) :
    """
    Given a map of comprable things, find all adjacent pairs of lines (first index) that are exactly the same.
    :param map:
    :return:
    """
    to_return = set( )

    for line_idx in range(map.shape[0]-1) :
        if numpy.all(map[line_idx,:] == map[line_idx+1,:]) :
            to_return.add((line_idx, line_idx+1),)

    return to_return

def is_reflection_point (map, line1_idx, line2_idx,) :
    """
    Given a map of comparable things, is the line in between line1_idx and line2_idx a reflection point across the map?
    Note: we expect line1_idx and line2_idx to be adjacent.
    :param map:
    :param line1_idx:
    :param line2_idx:
    :return:
    """

    # figure out the minimum width of the two rectangles we can make
    min_width = min(line1_idx+1, map.shape[0]-line2_idx)

    # cut out the two rectangles and flip one so they're directly comparable
    rec1 =            map[line1_idx-min_width+1:line1_idx+1,:]
    rec2 = numpy.flip(map[line2_idx:line2_idx+min_width,:], axis=0)

    #print("map shape: " + str(map.shape))
    #print("rec1 shape: " + str(rec1.shape))
    #print("rec2 shape: " + str(rec2.shape))

    #print("rec1: " )
    #print_map(rec1, indent=1,)
    #print("rec2: " )
    #print_map(rec2, indent=1,)

    # are our two rectangles the same?
    return numpy.all(rec1 == rec2)

# try to find the mirrors in each map
summary = 0
for tmp_key in sorted(all_maps.keys()) :
    print("Analyzing map: \n")
    print_map(all_maps[tmp_key], indent=1,)
    print()

    # analyze the lines
    curr_array = all_maps[tmp_key]
    potential_ref_points = find_equal_adjacent_lines(curr_array)
    print("Found potential reflection lines: " + str(potential_ref_points))
    found_point = None
    for tmp_pt in potential_ref_points :
        if is_reflection_point(curr_array, tmp_pt[0], tmp_pt[1],) :
            found_point = tmp_pt
            print("*** Found reflection point: " + str(found_point))
            summary += 100 * tmp_pt[1]

    # if we haven't found the reflection point, analyze the columns
    if found_point is None :
        folded_curr_array = curr_array.transpose()
        potential_ref_points = find_equal_adjacent_lines(folded_curr_array)
        print("Found potential reflection columns: " + str(potential_ref_points))
        for tmp_pt in potential_ref_points :
            if is_reflection_point(folded_curr_array, tmp_pt[0], tmp_pt[1],) :
                found_point = tmp_pt
                print("*** Found reflection point: " + str(found_point))
                summary += tmp_pt[1]

print("summary: " + str(summary))

"""
With my inputs: 31265

Time for part 1: 2:02
"""