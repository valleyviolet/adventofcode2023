#!/usr/bin/env python
# encoding: utf-8
"""
By Eva Schiffer
for the advent of code
"""

import re, sys
import numpy

#print("Args: " + str(sys.argv))
day_txt = "5"
file_to_open = "./input/day"+day_txt+"_input_test.txt" if len(sys.argv) > 1 and sys.argv[1] == "test" else "./input/day"+day_txt+"_input.txt"
file_obj = open(file_to_open)
data = file_obj.readlines()

NUMBERS_EXP = r'\d+'

MAX_VAL = numpy.iinfo(numpy.int64).max # there are other ways to handle this, but this should be ok

# Parse input to find
#       the list of seed ranges
#           each pair is the start and length of a range of seed ids
#       a bunch of maps
#           seed-to-soil,
#           soil-to-fertilizer,
#           fertilizer-to-water,
#           water-to-light,
#           light-to-temperature,
#           temperature-to-humidity,
#           humidity-to-location

# all maps work the same way. They're lists of 3 numbers in the form:
#       destination range start, source range start, range length
# so the src and dest ranges are the same, this is only an offset
# you map numbers only if they're inside the source range

# loop through all lines
seed_list = None
maps = { }
line_idx = 0
#print("data: \n" + str(data))
line_idx = 0

# parse seeds
seed_list = numpy.array(re.findall(NUMBERS_EXP, data[line_idx],), dtype=numpy.int64,)
len_seeds = seed_list.size
seed_list = numpy.reshape(seed_list, (int(len_seeds/2), 2), )
line_idx += 2

# look through the rest of the file
while line_idx <= len(data) :

    # setup stuff
    map_name = data[line_idx].split(" ")[0]
    tmp_nums = None
    line_idx += 1

    # parse all lines in the map
    while line_idx < len(data) and len(data[line_idx]) > 1 :
        this_row = numpy.array(re.findall(NUMBERS_EXP, data[line_idx],), dtype=numpy.int64,)
        tmp_nums = [this_row,] if tmp_nums is None else numpy.append(tmp_nums, [this_row,], axis=0,)
        line_idx += 1

    maps[map_name] = tmp_nums

    line_idx += 1

#print("Seeds: " + str(seed_list) )
print("Seeds shape: " + str(seed_list.shape) + "\n")
#print("Maps: " + str(maps))

class RangeHolder :
    def __init__(self) :
        """
        Make an empty range holder.
        """

        self.array = None

    def add_values (self, two_d_list_of_values,) :
        """
        Add the given values to our internal list
        :param two_d_list_of_values:
        :return:
        """

        if self.array is None :
            self.array = numpy.copy(two_d_list_of_values)
        else :
            self.array = numpy.append(self.array, two_d_list_of_values, axis=0, )

def intersection (a_start, a_length, b_start, b_length,) :
    """
    Given two ranges of integer numbers, return the intersection between them in the form:
        intersection_ab_start, intersection_ab_length
    (or None if the two don't intersect)
    :param a_start:
    :param a_length:
    :param b_start:
    :param b_length:
    :return:
    """
    a_end = a_start + a_length
    b_end = b_start + b_length

    # if we don't intersect!
    if b_start > a_end or a_start > b_end :
        return None

    i_start = max(a_start, b_start)
    i_end = min(a_end, b_end)
    i_length = i_end - i_start

    # if our length is 0, then we don't intersect
    if i_length == 0 :
        return None

    return i_start, i_length

def apply_map (input_array, map_array, ) :
    """
    Given an input array and a map, figure out what the output is after mapping.
    Note: as of part 2, the input is sets of [start_value, range_size,] rather than concrete numbers.

    :param input_array: first dimension is which range, second is start_value or range_size
    :param map_array:
    :return:
    """
    output_array = None

    # map each input value separately
    for i in range(input_array.shape[0]) :
        # get our current value
        this_start, this_range = input_array[i]
        this_end = this_start + this_range # for convenience

        this_out = RangeHolder() # this is where we'll put completely mapped ranges
        remains  = RangeHolder()

        # check the first line in the map (then we'll recurse
        r_dst_start, r_src_start, r_size = map_array[0]
        r_src_end = r_src_start + r_size  # for convenience
        # print("Checking map line: " + str(map_array[r_idx]))

        # check before the map range
        before_i = intersection(this_start, this_range, 0, r_src_start,)
        if before_i is not None :
            remains.add_values([before_i,], )

        # check inside the map range
        inside_i = intersection(this_start, this_range, r_src_start, r_size,)
        if inside_i is not None :
            this_out.add_values([[inside_i[0]-r_src_start+r_dst_start, inside_i[1],],])

        # check after the map range
        after_src_r = [r_src_start + r_size, MAX_VAL - (r_src_start + r_size + 1),]
        after_i = intersection(this_start, this_range, after_src_r[0], after_src_r[1],)
        if after_i is not None :
            remains.add_values([after_i,],)

        # if we have anything that didn't get matched, test it against the rest of the rules
        if remains.array is not None :
            if len(map_array) > 1 :
                this_out.add_values(apply_map(remains.array, map_array[1:],))
            else :
                this_out.add_values(remains.array,)

        output_array = this_out.array if output_array is None else numpy.append(output_array, this_out.array, axis=0, )

    return output_array

apply_order = [ 'seed-to-soil',
                'soil-to-fertilizer',
                'fertilizer-to-water',
                'water-to-light',
                'light-to-temperature',
                'temperature-to-humidity',
                'humidity-to-location']

# Apply the various maps
results = { }
previous_results = seed_list
for map_name in apply_order :
    print("Applying map: " + map_name)
    #print("Inputs: " + str(previous_results))
    new_results = apply_map(previous_results, maps[map_name],)
    results[map_name.split("-")[-1]] = new_results
    previous_results = new_results
    #print("Results: " + str(previous_results))

print("Available after mapping: " + str(results.keys()))
#print("Locations: " + str(results["location"]))

# find the smallest location number
min_location = numpy.min(results["location"][:,0])

print("Smallest location number: " + str(min_location))

"""
With my inputs: 41222968

(I didn't time this extra version of part 2.)
"""