#!/usr/bin/env python
# encoding: utf-8
"""
By Eva Schiffer
for the advent of code
"""

import re, sys

#print("Args: " + str(sys.argv))
import numpy

day_txt = "12"
file_to_open = "./input/day"+day_txt+"_input_"+sys.argv[1]+".txt" if len(sys.argv) > 1 else "./input/day"+day_txt+"_input.txt"
file_obj = open(file_to_open)
data = file_obj.readlines()

# Parse input to find
#       lines with springs map and checksum numbers

"""
Each line is a map of hotsprings that work and are broken, followed by a set of checksum numbers
    example: "???.### 1,1,3"
    
    ? means, the map was damaged, this hot spring could be fixed or broken
    . means the hotspring is operational
    # means the hotspring is damaged

The check sum is like a picross number set. It gives the size of each contiguous group of damaged springs.
So "1,3" means "(0 or more operational) 1 damaged (1 or more operational) 3 damaged ones (0 or more operational)".
Much like in picross, the contigious groups must be separated by some of the other state (or the contigious groups
we have would be grouped together and be fewer/larger).

Most importantly, we know the length of the array and where the "unknowns" are, but not how the groups of
damaged springs are placed. 
"""

NUMBERS_EXP = r'\d+' # if we're only expecting positive numbers

# parse our input
lines_info = { } # note, as they currently stand, these don't need to be ordered, but I'm going to give them indexes for simplicity
line_idx = 0
for line_txt in data :
    tmp = line_txt.strip().split(" ")
    tmp_nums = re.findall(NUMBERS_EXP, tmp[1])
    # because doing regexes with '.' and '?' is hard, replace '.' with 'o' (operational) and '?' with 'u' (unknown)
    tmp_txt = tmp[0].replace('.', 'o',).replace('?', 'u',)
    lines_info[line_idx] = (tmp_txt, numpy.array(tmp_nums, dtype=numpy.int16))
    line_idx += 1

# visualize input to make sure we parsed it correctly
print("Parsed the following lines: ")
for tmp_key in sorted(lines_info.keys()) :
    print("\t" + str(lines_info[tmp_key]))
    #tmp_arr = lines_info[tmp_key][1]
    #print("\t\toverall str length: " + str(len(lines_info[tmp_key][0])))
    #print("\t\tminimum chars to solve: " + str(numpy.sum(tmp_arr) + tmp_arr.size - 1))

# build custom regexes to find all the possible placements
"""
[#u] is a broken spring or an unknown spring
[uo] is an operational spring or an unknown spring

For each section of broken springs we need to manaually ask for that many characters plus our minimum one separator
for a 1 sized: (?:[#u][uo])
for a 5 sized: (?:[#u][#u][#u][#u][#u][uo])

The whole thing needs to be wrapped in a non capturing group:
    (?=(<whatever junk>))

So for example, the exp for length 2 looks like:
r = r'(?=((?:[#u][#u][uo])))'
"""
def make_custom_regex (to_find_size, include_separator=True,) :
    """
    Given how long our string to search is and the section we're trying to find,
    make a custom regex to find the leftmost section
    :param section_size:
    :return:
    """
    separator = '[uo]' if include_separator else ''
    broken_section = '(?:' + ('[#u]' * to_find_size) + separator + ')'

    return '(?=(' + broken_section + '))'

def find_all_with_positions (expression, to_look_in, ) :
    """
    Given an expression and a string to search, find the positions and matches.
    :param expression:
    :param to_look_in:
    :return:
    """

    found = set()
    for foundThing in re.finditer(expression, to_look_in) :
        match_txt = foundThing.group()
        found.add((foundThing.start(), match_txt))

    return found

def count_possible_matches(damaged_text, list_of_sizes,) :
    """
    Given damaged text that must contain sections of damaged springs in the sizes and orders in list_of_sizes,
    how many ways can we do that?
    :param damaged_text:
    :param list_of_sizes:
    :return:
    """

    #print("Analyzing text \"" + damaged_text + "\" with list: " + str(list_of_sizes))

    min_size_of_right_stuff = (numpy.sum(list_of_sizes[1:]) + len(list_of_sizes) - 2) if len(list_of_sizes) > 1 else 0
    tmp_regex = make_custom_regex(list_of_sizes[0], include_separator=len(list_of_sizes)>1,)
    tmp_text = damaged_text[:-1*min_size_of_right_stuff] if min_size_of_right_stuff > 0 else damaged_text
    potential_local_solutions = find_all_with_positions(tmp_regex, tmp_text,)

    # filter out the solutions that have '#' to the left of them
    actual_solutions = [ ]
    #print("\tPotential solutions: " + str(potential_local_solutions))
    for potential_solution in potential_local_solutions :
        st_idx = potential_solution[0]
        if st_idx == 0 :
            actual_solutions.append(st_idx, )
        elif '#' not in damaged_text[:st_idx] :
            actual_solutions.append(st_idx, )
    #print("\tActual solutions: " + str(actual_solutions))

    to_return = 0
    if len(list_of_sizes) <= 1 :
        # also exclude solutions where there's unmatched hashes to the right
        tmp_solutions = [ ]
        for st_idx in actual_solutions :
            remainder = damaged_text[st_idx+list_of_sizes[0]:]
            if '#' not in remainder :
                tmp_solutions.append(st_idx)
        #print("\tonly " + str(len(list_of_sizes)) + " sections left, returning: " + str(len(tmp_solutions)))
        to_return = len(tmp_solutions)
    else :
        new_list = list_of_sizes[1:]
        left_size = list_of_sizes[0] + 1
        #print("\tAnalyzing local solutions: " + str(actual_solutions))
        #print("\tnew list: " + str(new_list))
        for tmp_solution in actual_solutions :
            new_text = damaged_text[left_size+tmp_solution:]
            #print("\tnew text: " + new_text)
            to_return += count_possible_matches(new_text, new_list,)

    return to_return

# figure out how many solutions each one has
print("Counting possible solutions: ")
sum_possible = 0
for tmp_key in sorted(lines_info.keys()) :
    print("Solving for possible solutions of: " + str(lines_info[tmp_key]))
    tmp_num_solutions = count_possible_matches(lines_info[tmp_key][0], lines_info[tmp_key][1])
    print("\tfound solutions: " + str(tmp_num_solutions))
    sum_possible += tmp_num_solutions

print("Total possible solutions found: " + str(sum_possible))

"""
I initially tried a strategy of matching the first and last things at the same time, but there isn't a
way I could find to force re to give you all the possible interpretations for the spacing of the 
operational sections of springs. You can make it choose between greedy and stingy by manually forming
the sections for the operational pieces.

So with 1 to 3 spaces available:
    greedy: (?:[uo][uo][uo]|[uo][uo]|[uo])
    stingy: (?:[uo]|[uo][uo]|[uo][uo][uo])
But it won't ever be "all possible".
"""

"""
With my inputs: 7251

Time for part 1: Ugh, so long
"""