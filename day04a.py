#!/usr/bin/env python
# encoding: utf-8
"""
By Eva Schiffer
for the advent of code
"""

import re

#file_obj = open("./input/day4_input_test.txt")
file_obj = open("./input/day4_input.txt")
data = file_obj.readlines()

NUMBERS_EXP = r'\d+'

# Parse input to find
#       the potentially winning numbers
#       the numbers this card has

line_idx = 0
all_cards_winning = { }
all_cards_havenum = { }
# each card will be represented by an id used as key and entries in the two dicts
# the ...winning dict has a set of the winning numbers for that card
# the ...havenum dict has the numbers that are on that card (that might cause it to win)

# loop through all lines
for line_txt in data :

    # we expect each line to look like
    #   "Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53"
    # where the two lists are separated by | and represent
    # a list of winning numbers and then a list of numbers you have

    card_id, w_nums, h_nums = re.split(r"[:|]", line_txt)
    all_cards_winning[card_id] = set(re.findall(NUMBERS_EXP, w_nums,))
    all_cards_havenum[card_id] = re.findall(NUMBERS_EXP, h_nums,)

#print("Found cards: " +  + "\n")

def calc_points (winning_nums_on_card, have_nums_on_card, ) :
    """
    Given a set of winning numbers on a card and a list of the numbers that are scorable on the card,
    calculate it's point value.
    The scoring rules given are:
    "The first match makes the card worth one point and each match after the first doubles the point value of that card."
    (But I expect that will change in part 2.)
    :param winning_nums_on_card:
    :param have_nums_on_card:
    :return:
    """

    points = 0

    for tmp_num in have_nums_on_card :
        if tmp_num in winning_nums_on_card :
            points = 1 if points == 0 else points * 2

    return points

# Score and sum the cards' points
total_vals = 0
for card_id in all_cards_winning :

    total_vals += calc_points(all_cards_winning[card_id], all_cards_havenum[card_id],)

print("Total point value: " + str(total_vals))

"""
With my inputs: 25010

Time for part 1: 0:20
"""