#!/usr/bin/env python
# encoding: utf-8
"""
By Eva Schiffer
for the advent of code
"""

import re, sys
import numpy

#print("Args: " + str(sys.argv))
day_txt = "6"
file_to_open = "./input/day"+day_txt+"_input_test.txt" if len(sys.argv) > 1 and sys.argv[1] == "test" else "./input/day"+day_txt+"_input.txt"
file_obj = open(file_to_open)
data = file_obj.readlines()

NUMBERS_EXP = r'\d+'

MAX_VAL = numpy.iinfo(numpy.int64).max # there are other ways to handle this, but this should be ok

# Parse input to find
#       a time
#       a distance
# oops bad kerning, just one number each!

# times are in ms; distances are in mm

# parse our input
times_list = [''.join(re.findall(NUMBERS_EXP, data[0],), ),]
dists_list = [''.join(re.findall(NUMBERS_EXP, data[1],), ),]

print("Times:     " + str(times_list) )
print("Distances: " + str(dists_list) )

# The equation that describes our movement is:
#       distance = time_held * (total_time - time_held)
#
# where the total time for a race is known and how long
# the button is held is our choice. The minimum distance
# to beat is also known.
# there are going to be multiple solutions where our
# distance is > the minimum distance.

def calc_distance_traveled (speed, time,) :
    """
    Given a speed (which in our case is the same as the time we hold the button) calculate how
    far our boat will travel in the given time.
    :param speed:
    :return:
    """

    return speed * time

# calculate the possible holding times/speeds
ways_to_win = [ ]
total_win = 1
for race_num in range(len(times_list)) :
    r_time     = int(times_list[race_num])
    r_min_dist = int(dists_list[race_num])
    print("race time: " + str(r_time))

    to_win_this = 0
    for t in range(r_time+1,) :
        tmp_dist = calc_distance_traveled(t, r_time-t,)
        to_win_this = to_win_this if tmp_dist <= r_min_dist else to_win_this + 1

    ways_to_win.append(to_win_this,)
    total_win *= to_win_this

print("Number of ways to win: " + str(ways_to_win))
print("Multiplication of ways to win: " + str(total_win))

"""
With my inputs: 35349468

Time for part 1: 0:28
Time for part 2: 0:06
Total time for this day: 0:34
"""