#!/usr/bin/env python
# encoding: utf-8
"""
By Eva Schiffer
for the advent of code
"""

import re, sys
import numpy

#print("Args: " + str(sys.argv))
day_txt = "9"
file_to_open = "./input/day"+day_txt+"_input_"+sys.argv[1]+".txt" if len(sys.argv) > 1 else "./input/day"+day_txt+"_input.txt"
file_obj = open(file_to_open)
data = file_obj.readlines()

NUMBERS_EXP = r'-?\d+' # ? is zero or 1 times, so numbers can now be negative

# Parse input to find
#       a set of increasing numbers

# note, unlike previous inputs, these may contain negative numbers

# parse our input
numbers = { }
arr_idx = 0
for line_txt in data :
    tmp = list(numpy.array(re.findall(NUMBERS_EXP, line_txt), dtype=numpy.int64,))
    numbers[arr_idx] = tmp
    arr_idx += 1

print("Found " + str(len(numbers.keys())) + " input number lists." )
#for n_list_idx in sorted(numbers.keys()) :
#    print("\t" + str(numbers[n_list_idx]))

# figure out how the lists are increasing and what the "next" number in each list will be
sum_new_values = 0

# examine each of our number lists separately
for l_idx in range(len(numbers.keys())) :
    tmp_list = numbers[l_idx]
    print("Analyzing number list: " )
    print(str(tmp_list))

    # find manual derivatives until we get to 0
    this_d = { }
    d_idx = 0
    this_d[d_idx] = tmp_list
    while numpy.count_nonzero(this_d[d_idx]) > 0 :
        tmp_list = this_d[d_idx]
        d_idx += 1
        # calculate the differences
        this_d[d_idx] = [tmp_list[tmp_idx] - tmp_list[tmp_idx-1] for tmp_idx in range(1, len(tmp_list))]
        #print(str(this_d[d_idx]))

    # now go backwards to find our next value
    #print("Reconstructing new values: ")
    tmp_sum_val = 0
    for key_n in reversed(sorted(this_d.keys())) :
        tmp_arr = this_d[key_n]
        tmp_sum_val = tmp_arr[0] - tmp_sum_val
        tmp = [tmp_sum_val].append(tmp_arr)
        #print(str(tmp))

    # hang on to our final calculated sum value
    sum_new_values += tmp_sum_val
    print("Adding " + str(tmp_sum_val) + " to our running total to get: " + str(sum_new_values))

print("Sum of extrapolated values: " + str(sum_new_values)) #

"""
With my inputs: 905

Time for part 1: 1:51
Time for part 2: 0:22

Total time for this day: 2:13
"""