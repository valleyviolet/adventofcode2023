#!/usr/bin/env python
# encoding: utf-8
"""
By Eva Schiffer
for the advent of code
"""

import re, sys, os

NUMBERS_EXP = r'-?\d+' # ? is zero or 1 times, so numbers can now be negative

#print("Args: " + str(sys.argv))
day_txt = re.findall(NUMBERS_EXP, os.path.basename(vars(sys.modules[__name__])['__file__']))[0]
file_to_open = "./input/day"+day_txt+"_input_"+sys.argv[1]+".txt" if len(sys.argv) > 1 else "./input/day"+day_txt+"_input.txt"
file_obj = open(file_to_open)
data = file_obj.readlines()

# Parse input to find
#       a hex code that encodes a dig command

"""
The direction is U, D, L, or R which corresponds to UP DOWN LEFT RIGHT but also basically
    UP    = North
    DOWN  = South
    LEFT  = West
    RIGHT = East

The first 5 hex digits are how many spaces to dig.

The last hex digit is the direction:
    0 means R
    1 means D
    2 means L
    3 means U

"""

UP    = "U"
DOWN  = "D"
LEFT  = "L"
RIGHT = "R"
TO_GO = {
            UP:     (-1,  0),
            DOWN:   ( 1,  0),
            LEFT:   ( 0, -1),
            RIGHT:  ( 0,  1),
        }
DIR_CONVERT =   {
                    "0": RIGHT,
                    "1": DOWN,
                    "2": LEFT,
                    "3": UP,
                }

# parse our input
tmp_ln_num = 0
tmp_col_num = 0
all_lns = set( )
all_cols = set( )
dug_vertexes = [ (0,0), ] # we're just going to take (0,0) as a given since we start there
num_ints_on_edge = 0
for ln_txt in data :
    tmp = ln_txt.split("#")[-1][:-2]
    dir = DIR_CONVERT[tmp[-1]]
    dist = int(tmp[0:-1], 16,)
    #print("Command(" + tmp + "): " + dir + " " + str(dist))
    ln_increment = TO_GO[dir][0]
    cl_increment = TO_GO[dir][1]
    tmp_ln_num  += ln_increment * dist
    tmp_col_num += cl_increment * dist
    dug_vertexes.append((tmp_ln_num, tmp_col_num),)
    all_lns.add(tmp_ln_num)
    all_cols.add(tmp_col_num)
    num_ints_on_edge += abs(ln_increment * dist) + abs(cl_increment * dist)
tmp_lines = sorted(list(all_lns))
min_line = tmp_lines[0]
max_line = tmp_lines[-1]
tmp_cols = sorted(list(all_cols))
min_col  = tmp_cols[0]
max_col  = tmp_cols[-1]
#num_ints_on_edge -= 1 # don't double count (0,0)

print("Found data range (" + str(min_line) + " to " + str(max_line) + ", " + str(min_col) + " to " + str(max_col) + ")")
print("Found vertexes of dug area: " + str(dug_vertexes))

# make all our vertexes have positive coordinates
#for v_idx in range(len(dug_vertexes)) :
#    dug_vertexes[v_idx] = (dug_vertexes[v_idx][0] - min_line, dug_vertexes[v_idx][1] - min_col)
#print("Found vertexes of dug area: " + str(dug_vertexes))

# now use Pick's Theorem and the Shoelace Formula to find the area?

"""
Note: I didn't work this out myself because frankly despite having multiple higher calc classes in 
college I don't think I've ever heard of either of these things. I got a hint because I wasn't going 
to f'ing reinvent mathematics to figure out this problem. 

Anyway, Pick's theorem is to do with polygons with only integer vertex coordinates. It says

    area_of_the_polygon = nun_int_points_inside_the_polygon + (num_int_points_on boundary / 2) - 1

The Shoelace formula says that we can use the trapesoid formula for each pair of adjacent points:
    
    polygon area = 1/2 * sum ( (yi + y(i+1)) * (xi - x(i+1)) )
    
    (sum for all adjacent points i and i+1 )
"""

tmp_sum = 0
for v_idx in range(len(dug_vertexes)-1) :
    tmp_sum += (dug_vertexes[v_idx][0] + dug_vertexes[v_idx+1][0]) * (dug_vertexes[v_idx][1] - dug_vertexes[v_idx+1][1])
tmp_sum = tmp_sum / 2.0
print("Area of lagoon: " + str(tmp_sum))
print("Num int points on edges: " + str(num_ints_on_edge))
tmp = tmp_sum + 1 - (num_ints_on_edge / 2)

print("Number of dug square meters: " + str(tmp + num_ints_on_edge))

"""
With my inputs: 134549294799713

This is an awful puzzle. 
"""