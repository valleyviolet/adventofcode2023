#!/usr/bin/env python
# encoding: utf-8
"""
By Eva Schiffer
for the advent of code
"""

import numpy, re

file_obj = open("./input/day2_input.txt")
data = file_obj.readlines()

EXPECTED_COLORS = ["red", "green", "blue",]

def contains_str(to_search, to_find, ) :
    """
    Given a string to search and text to find in it, return true if to_search contains to_find, otherwise false.

    :param to_search:
    :param to_find:
    :return:
    """
    return len(re.findall(to_find, to_search)) > 0

all_games = {}
for line_txt in data :

    #print("original line: " + line_txt)

    # we expect each line to look something like:
    #   "Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green"

    tmp = line_txt.split(":")
    game_id = int(''.join(re.findall(r'\d+', tmp[0])))
    #print("game id: " + str(game_id))

    # parse each game
    game_totals = {}
    for pull_txt in tmp[1].split(";") :
        colors_list = pull_txt.split(",")
        for color in colors_list :
            for find_color in EXPECTED_COLORS :
                if contains_str(color, find_color) :
                    num_seen = int(''.join(re.findall(r'\d+', color)))
                    if find_color in game_totals :
                        if game_totals[find_color] < num_seen :
                            game_totals[find_color] = num_seen
                    else :
                        game_totals[find_color] = num_seen
    #print("parsed game: " + str(game_totals) + "\n")
    all_games[game_id] = game_totals

print("Game totals: " + str(all_games) + "\n")

def calc_game_power(game_totals, ) :
    """
    Given a set of game totals, what is the total power of the cubes?

    :param game_totals:
    :return:
    """

    this_game_score = 1

    for tmp_color in game_totals :
        this_game_score *= game_totals[tmp_color]

    return this_game_score

total_power = 0
for game_tmp_id in all_games :
    game_tmp_power = calc_game_power(all_games[game_tmp_id],)
    print("Adding game " + str(game_tmp_id) + " power: " + str(game_tmp_power))
    total_power += game_tmp_power

print("Total power of games: " + str(total_power))

"""
With my inputs:
Total power of games: 70387

Total time for both parts = 45 min
"""