#!/usr/bin/env python
# encoding: utf-8
"""
By Eva Schiffer
for the advent of code
"""

import numpy, re

file_obj = open("./input/day1_input_a.txt")
data = file_obj.readlines()

code_list = numpy.array([], dtype=int,)
for line_txt in data :

    temp = ''.join(re.findall(r'\d+', line_txt))
    #print(temp)
    #print("start: " + temp[0])
    this_code = int(temp[0] + temp[-1])
    code_list = numpy.append(code_list, this_code,)

cal_sum = numpy.sum(code_list)

print("code list: " + str(code_list))
print("total: " + str(cal_sum))
