#!/usr/bin/env python
# encoding: utf-8
"""
By Eva Schiffer
for the advent of code
"""

import re, sys, os

NUMBERS_EXP = r'-?\d+' # ? is zero or 1 times, so numbers can now be negative

#print("Args: " + str(sys.argv))
day_txt = re.findall(NUMBERS_EXP, os.path.basename(vars(sys.modules[__name__])['__file__']))[0]
file_to_open = "./input/day"+day_txt+"_input_"+sys.argv[1]+".txt" if len(sys.argv) > 1 else "./input/day"+day_txt+"_input.txt"
file_obj = open(file_to_open)
data = file_obj.readlines()

# Parse input to find
#       TODO details

# TODO any more details of input

# parse our input

# visualize input to make sure we parsed it correctly

# TODO modify or examine our input to solve the puzzle

print("TODO puzzle solution goes here: " + str(""))

"""
With my inputs: 

Started part 1:
Finishd part 1:

Time for part 1: 
Time for part 2: 
Total time for this day: 
"""