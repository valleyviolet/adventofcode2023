#!/usr/bin/env python
# encoding: utf-8
"""
By Eva Schiffer
for the advent of code
"""

import re, sys, os

import numpy

NUMBERS_EXP = r'-?\d+' # ? is zero or 1 times, so numbers can now be negative

#print("Args: " + str(sys.argv))
day_txt = re.findall(NUMBERS_EXP, os.path.basename(vars(sys.modules[__name__])['__file__']))[0]
file_to_open = "./input/day"+day_txt+"_input_"+sys.argv[1]+".txt" if len(sys.argv) > 1 else "./input/day"+day_txt+"_input.txt"
file_obj = open(file_to_open)
data = file_obj.readlines()

# Parse input to find
#       a comma separated list of instructions

# note, we're ignoring new lines, commas, and (I think also) outer white space.

# parse our input
commands_txt = data[0].strip().split(',')

def holiday_hash (string_input,) :
    """
    Given a string, return the results of the "Holiday ASCII String Helper algorithm" on that string.

    Documentation for this algorithm:

        To run the HASH algorithm on a string, start with a current value of 0.
        Then, for each character in the string starting from the beginning:

            Determine the ASCII code for the current character of the string.
            Increase the current value by the ASCII code you just determined.
            Set the current value to itself multiplied by 17.
            Set the current value to the remainder of dividing itself by 256.

        After following these steps for each character in the string in order,
        the current value is the output of the HASH algorithm.

    :param string_input:
    :return: a single number in the range 0 to 255
    """
    curr_value = 0

    for char_tmp in list(string_input) :
        curr_value += ord(char_tmp)
        curr_value *= 17
        curr_value %= 256

    return curr_value

# visualize input to make sure we parsed it correctly
#print("input data: " + str(commands_txt))

# minimal test of our hash
#print("Hash of \'HASH\' (should be 52): " + str(holiday_hash('HASH')))

"""
we are going to keep all our lenses in a dictionary of lists
    all_boxes is a dictionary keyed by box number
    empty boxes have no entry
    boxes with something in them have a list of all lenses
        The "first" lense in a box is the one at index 0.
    each lense is represented by a tuple
            (label, focal length number)
"""

def remove_lense (box_number, label_to_use, all_boxes,) :
    """
    Remove a labeled lense from the box_numbered box, if it's present.

    Documentation for command:

        If the operation character is a dash (-),
        go to the relevant box and remove the lens with the given label
        if it is present in the box. Then, move any remaining lenses as
        far forward in the box as they can go without changing their order,
        filling any space made by removing the indicated lens. (If no lens
        in that box has the given label, nothing happens.)

    :param box_number:
    :param label_to_use:
    :param all_boxes:
    :return:
    """
    # if the box we're looking for is empty, stop now
    if box_number not in all_boxes :
        return

    tmp_box = all_boxes[box_number]
    # find the label in the box and remove it if possible
    where_idx = -1
    for idx in range(len(tmp_box)) :
        if tmp_box[idx][0] == label_to_use :
            where_idx = idx
    # if we found a matching lense in the box, remove it
    if where_idx >= 0 :
        if where_idx == 0 :
            if len(tmp_box) == 1 :
                del all_boxes[box_number]
            else :
                all_boxes[box_number] = tmp_box[1:]
        elif where_idx == (len(tmp_box)-1) :
            all_boxes[box_number] = tmp_box[:-1]
        else :
            tmp_arr = numpy.array(tmp_box, dtype=object,)
            new_arr = list(numpy.concatenate((tmp_arr[:where_idx], tmp_arr[where_idx+1:],),))
            all_boxes[box_number] = new_arr

def add_or_replace_lense (box_number, label_to_use, focal_length, all_boxes,) :
    """
    Add or replace the indicated lense as needed

    Documentation for command:

        If the operation character is an equals sign (=),
        it will be followed by a number indicating the focal length of the lens
        that needs to go into the relevant box; be sure to use the label maker
        to mark the lens with the label given in the beginning of the step so
        you can find it later. There are two possible situations:

        If there is already a lens in the box with the same label,
            replace the old lens with the new lens: remove the old lens and put
            the new lens in its place, not moving any other lenses in the box.
        If there is not already a lens in the box with the same label,
            add the lens to the box immediately behind any lenses already in the box.
            Don't move any of the other lenses when you do this. If there aren't any
            lenses in the box, the new lens goes all the way to the front of the box.

    :param box_number:
    :param label_to_use:
    :param focal_length:
    :param all_boxes:
    :return:
    """
    tmp_new_lense = (label_to_use, focal_length)

    if box_number in all_boxes :
        tmp_box = all_boxes[box_number]
        if label_to_use in str(tmp_box) :
            # need to replace the labeled lense
            for idx in range(len(tmp_box)) :
                if tmp_box[idx][0] == label_to_use :
                    tmp_box[idx] = tmp_new_lense
        else : # just add it to the back
            tmp_box.append(tmp_new_lense, )
    else : # if there's nothing in the box, just add it
        all_boxes[box_number] = [tmp_new_lense,]

def print_all_boxes (all_boxes, indent=0,) :
    """
    Given a set of boxes, print in a human readable way.
    :param all_boxes:
    :param indent:
    :return:
    """

    indent_txt = ""
    for i in range(indent):
        indent_txt += "\t"

    for key in sorted(all_boxes.keys()) :
        print(indent_txt + "Box " + str(key) + ": " + str(all_boxes[key]))

def calc_total_focusing_power(all_boxes,) :
    """
    Calculate the total focusing power for this set of boxes and lenses

        To confirm that all of the lenses are installed correctly,
        add up the focusing power of all of the lenses.
        The focusing power of a single lens is the result of multiplying together:
            One plus the box number of the lens in question.
            The slot number of the lens within the box: 1 for the first lens, 2 for the second lens, and so on.
            The focal length of the lens.

    :param all_boxes:
    :return:
    """
    sum_power = 0

    for box_key in all_boxes.keys() :
        tmp_box = all_boxes[box_key]
        for lens_idx in range(len(tmp_box)) :
            this_lens_power = (box_key + 1) * (lens_idx + 1) * int(tmp_box[lens_idx][1])
            sum_power += this_lens_power

            do_disp = True
            if do_disp :
                print("Box " + str(box_key) + " lense place " + str(lens_idx+1) + " focal length " + tmp_box[lens_idx][1]
                      + " = " + str(this_lens_power))

    return sum_power

# run all our input commands
all_boxes = { }
for command in commands_txt :
    tmp_label = command.split('-')[0].split('=')[0]
    box_number = holiday_hash(tmp_label)
    if '-' in command :
        remove_lense(box_number, tmp_label, all_boxes,)
    elif '=' in command :
        tmp_focal_length = command[-1]
        add_or_replace_lense(box_number, tmp_label, tmp_focal_length, all_boxes,)

    step_disp = True
    if step_disp :
        print("\nAfter \"" + command + "\": ")
        print_all_boxes(all_boxes, indent=1,)

# calculate the total focusing power in all our boxes
sum_focus = calc_total_focusing_power(all_boxes,)

print("Total focusing power: " + str(sum_focus))

"""
With my inputs: 268497

Time for part 1: 0:12
Time for part 2: 0:56
Total time for this day: 1:08
"""