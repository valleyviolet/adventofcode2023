#!/usr/bin/env python
# encoding: utf-8
"""
By Eva Schiffer
for the advent of code
"""

import numpy, re

file_obj = open("./input/day2_input.txt")
data = file_obj.readlines()

EXPECTED_COLORS = ["red", "green", "blue",]

def contains_str(to_search, to_find, ) :
    """
    Given a string to search and text to find in it, return true if to_search contains to_find, otherwise false.

    :param to_search:
    :param to_find:
    :return:
    """
    return len(re.findall(to_find, to_search)) > 0

all_games = {}
for line_txt in data :

    print("original line: " + line_txt)

    # we expect each line to look something like:
    #   "Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green"

    tmp = line_txt.split(":")
    game_id = int(''.join(re.findall(r'\d+', tmp[0])))
    print("game id: " + str(game_id))

    # parse each game
    game_totals = {}
    for pull_txt in tmp[1].split(";") :
        colors_list = pull_txt.split(",")
        for color in colors_list :
            for find_color in EXPECTED_COLORS :
                if contains_str(color, find_color) :
                    num_seen = int(''.join(re.findall(r'\d+', color)))
                    if find_color in game_totals :
                        if game_totals[find_color] < num_seen :
                            game_totals[find_color] = num_seen
                    else :
                        game_totals[find_color] = num_seen
    print("parsed game: " + str(game_totals) + "\n")
    all_games[game_id] = game_totals

print("Game totals: " + str(all_games) + "\n")

# 12 red cubes, 13 green cubes, and 14 blue cubes
test_against =  {
                    "red":   12,
                    "blue":  14,
                    "green": 13,
                }

def game_possible(game_totals, real_values,) :
    """
    Given a set of game totals, is it possible to have the values in real_values in the bag?

    :param game_totals:
    :param real_values:
    :return:
    """

    this_game_ok = True
    print("testing game: " + str(game_totals))
    for tmp_color in game_totals :
        if real_values[tmp_color] < game_totals[tmp_color] :
            #print("fail on color: " + tmp_color)
            this_game_ok = False

    return this_game_ok

total_ids = 0
for game_tmp_id in all_games :
    this_game_ok = game_possible(all_games[game_tmp_id], test_against,)
    print("Did game " + str(game_tmp_id) + " pass? " + str(this_game_ok))

    if this_game_ok :
        total_ids += game_tmp_id

print("Total of ok games' ids: " + str(total_ids))

"""
With my inputs:
Total of ok games' ids: 1734
"""