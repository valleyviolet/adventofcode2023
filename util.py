#!/usr/bin/env python
# encoding: utf-8
"""
By Eva Schiffer
for the advent of code
"""

import re
import numpy

""" 
Common ways to parse input
"""
data = [ ]
# if we need every line and they're all the same
for line_txt in data :
    pass # do stuff!
# if we need to manually step through sets of lines
line_idx = 0
while line_idx < len(data) :
    pass # do stuff, don't forget to increment line_idx!
# or in a more specific case of groups separated by blank lines
line_idx = 0
while line_idx < len(data) :
    # TODO setup data holder for this group
    # read one group
    while line_idx < len(data) and len(data[line_idx].strip()) > 0 :
        # TODO parse data for a single group
        line_idx += 1
    # TODO integerate this groups data with overall data
    line_idx += 1

# parsing numbers from strings
NUMBERS_EXP = r'-?\d+' # ? is zero or 1 times, so numbers can now be negative
NUMBERS_EXP = r'\d+' # if we're only expecting positive numbers
# get all numbers on a line
line_txt = ""
tmp = re.findall(NUMBERS_EXP, line_txt)
# shove those all together
tmp_all = ''.join(tmp)

# this isn't guaranteed, but seems to be how he usually formats directions
# (Note, my indexing is (line, column) here)
NORTH = "north"
SOUTH = "south"
EAST  = "east"
WEST  = "west"
TO_GO = {
            NORTH:  (-1,  0),
            SOUTH:  ( 1,  0),
            WEST:   ( 0, -1),
            EAST:   ( 0,  1),
        }

# changing number base formatting with strings
tmp_str = "0001110" # a binary number as string
tmp_int_val = int(tmp_str, 2,) # in this case 2 is the base
# this will give me a regular base 10 integer with that binary value
# if I want to do the reverse, look at the solutions here:
#   https://stackoverflow.com/questions/2063425/python-elegant-inverse-function-of-intstring-base

# count letters in a piece of text
from collections import Counter
test_text = "some text to count letters in"
letter_counts = dict(Counter(test_text))

# use a custom comparitor in a search
from functools import cmp_to_key
def comp_fn (input1, input2,) :
    """
    Comparator function
    :param input1:
    :param input2:
    :return: -1 if input1 one is smaller, 1 if input1 is bigger or 0 if the two inputs are equal
    """
to_sort = [3, 5, 2, 4,]
sorted_things = sorted(to_sort, key=cmp_to_key(comp_fn()),)
# I probably need to learn how to make a proper key function instead.

def replace_txt (input_str, replace_table,) :
    """
    Given a line of text, replace any text that appears as a key
    in replace_table with the value at that key in the table
    :param input_str:
    :param replace_table A dictionary where the keys are what to replace and the values are what to replace it with.
    :return:
    """
    out_str = input_str

    for key in replace_table :
        value = replace_table[key]
        out_str = out_str.replace(key, value,)

    return out_str

def contains_str(to_search, to_find, ) :
    """
    Given a string to search and text to find in it, return true if to_search contains to_find, otherwise false.

    :param to_search:
    :param to_find:
    :return:
    """
    return len(re.findall(to_find, to_search)) > 0

def find_all_with_positions (expression, to_look_in, exclude=None,) :
    """
    Given an expression and a string to search, find the positions and matches that aren't in the exclude list.
    :param expression:
    :param to_look_in:
    :param exclude:
    :return:
    """

    if exclude is None :
        exclude = set()

    found = set()

    for foundThing in re.finditer(expression, to_look_in) :
        match_txt = foundThing.group()
        if match_txt not in exclude :
            found.add((foundThing.start(), match_txt))

    return found

# if you don't need to exclude anything
def find_all_with_positions (expression, to_look_in, ) :
    """
    Given an expression and a string to search, find the positions and matches.
    :param expression:
    :param to_look_in:
    :return:
    """

    found = set()
    for foundThing in re.finditer(expression, to_look_in) :
        match_txt = foundThing.group()
        found.add((foundThing.start(), match_txt))

    return found

def generate_surrounding_idxes (line_idx, col_idx, ) :
    """
    Given a line and column index, return the tuples of all the surrounding indexes,
    including the input index and any directly or diagonally adjacent.

    :param line_idx:
    :param col_idx:
    :return:
    """

    to_return = set( )
    for l_idx in range(line_idx-1, line_idx+2,) :
        for c_idx in range(col_idx-1, col_idx+2,) :
            to_return.add ((l_idx, c_idx,))

    return to_return

class RangeHolder :
    def __init__(self) :
        """
        Make an empty range holder.
        """

        self.array = None

    def add_values (self, two_d_list_of_values,) :
        """
        Add the given values to our internal list
        :param two_d_list_of_values:
        :return:
        """

        if self.array is None :
            self.array = numpy.copy(two_d_list_of_values)
        else :
            self.array = numpy.append(self.array, two_d_list_of_values, axis=0, )

def intersection (a_start, a_length, b_start, b_length,) :
    """
    Given two ranges of integer numbers, return the intersection between them in the form:
        intersection_ab_start, intersection_ab_length
    (or None if the two don't intersect)
    :param a_start:
    :param a_length:
    :param b_start:
    :param b_length:
    :return:
    """
    a_end = a_start + a_length
    b_end = b_start + b_length

    # if we don't intersect!
    if b_start > a_end or a_start > b_end :
        return None

    i_start = max(a_start, b_start)
    i_end = min(a_end, b_end)
    i_length = i_end - i_start

    # if our length is 0, then we don't intersect
    if i_length == 0 :
        return None

    return i_start, i_length


def solve_quadratic(a_val, b_val, c_val, ):
    """
    Given concrete a, b, and c, return the two possible solutions to the quadratic equation.
    We're using the quadratic equation:
            x = ( -b ± sqrt( b^2 - 4ac) ) / 2a

    relearning the quadratic equation:
    https://www.khanacademy.org/test-prep/sat/x0a8c2e5f:untitled-652/x0a8c2e5f:passport-to-advanced-math-lessons-by-skill/a/gtp--sat-math--article--solving-quadratic-equations--lesson

    :param a_val:
    :param b_val:
    :param c_val:
    :return: The two solutions in ascending order.
    """
    tmp_a = float(a_val)
    tmp_b = float(b_val)
    tmp_c = float(c_val)

    tmp_root = numpy.sqrt((tmp_b * tmp_b) - (4 * tmp_a * tmp_c))
    x1 = ((-1 * tmp_b) + tmp_root) / (2 * tmp_a)
    x2 = ((-1 * tmp_b) - tmp_root) / (2 * tmp_a)

    return min(x1, x2, ), max(x1, x2, )

def print_map(map_array, indent=0, true_char='#', false_char='.', ) :
    """
    Given a map info that is a string or bool, print it in a user readable way.
    :param map_array:
    :return:
    """

    indent_txt = ""
    for i in range(indent) :
        indent_txt += "\t"

    for l_idx in range(map_array.shape[0]) :
        this_line = indent_txt
        for c_idx in range(map_array.shape[1]) :
            if map_array.dtype == bool :
                this_line += true_char if map_array[l_idx,c_idx] else false_char
            else :
                this_line += str(map_array[l_idx,c_idx])
        print(this_line)

def print_map(map_array, indent=0, true_char='#', false_char='.', extra_stuff_positions=None, ) :
    """
    Given a map info that is a string or bool, print it in a user readable way.
    :param map_array:
    :param indent:
    :param true_char:
    :param false_char:
    :param extra_stuff_positions:
    :return:
    """
    extra_stuff_positions = set( ) if extra_stuff_positions is None else extra_stuff_positions

    indent_txt = ""
    for i in range(indent) :
        indent_txt += "\t"

    for l_idx in range(map_array.shape[0]) :
        this_line = indent_txt
        for c_idx in range(map_array.shape[1]) :
            if map_array.dtype == bool :
                tmp_char = true_char if map_array[l_idx,c_idx] else false_char
                                                                                        # invert extra stuff
                tmp_char = tmp_char if (l_idx,c_idx) not in extra_stuff_positions else "\033[7m" + tmp_char + "\033[0m"
                this_line += tmp_char
            else :
                tmp_char = str(map_array[l_idx,c_idx])
                                                                                        # invert extra stuff
                tmp_char = tmp_char if (l_idx, c_idx) not in extra_stuff_positions else "\033[7m" + tmp_char + "\033[0m"
                this_line += tmp_char
        print(this_line)