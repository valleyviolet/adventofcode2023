#!/usr/bin/env python
# encoding: utf-8
"""
By Eva Schiffer
for the advent of code
"""

import re

#file_obj = open("./input/day4_input_test.txt")
file_obj = open("./input/day4_input.txt")
data = file_obj.readlines()

NUMBERS_EXP = r'\d+'

# Parse input to find
#       the potentially winning numbers
#       the numbers this card has

line_idx = 0
all_cards_winning = { }
all_cards_havenum = { }
card_list = [ ]
# each card will be represented by an id used as key and entries in the two dicts
# the ...winning dict has a set of the winning numbers for that card
# the ...havenum dict has the numbers that are on that card (that might cause it to win)
# There is also a card_list, that keeps track of the ordering of the cards.

# loop through all lines
for line_txt in data :

    # we expect each line to look like
    #   "Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53"
    # where the two lists are separated by | and represent
    # a list of winning numbers and then a list of numbers you have

    card_id, w_nums, h_nums = re.split(r"[:|]", line_txt)
    all_cards_winning[card_id] = set(re.findall(NUMBERS_EXP, w_nums,))
    all_cards_havenum[card_id] = re.findall(NUMBERS_EXP, h_nums,)
    card_list.append(card_id,)

print("Found cards: " + str(card_list) + "\n")

def calc_points (winning_nums_on_card, have_nums_on_card, ) :
    """
    Given a set of winning numbers on a card and a list of the numbers that are scorable on the card,
    calculate it's point value.
    The scoring rules given are:
    You get one point for each matching "winning" number in your numbers.
    :param winning_nums_on_card:
    :param have_nums_on_card:
    :return:
    """

    points = 0

    for tmp_num in have_nums_on_card :
        if tmp_num in winning_nums_on_card :
            points += 1

    return points

# Score and sum the cards
cards_won_by_card = { }
# evaluate the list in reverse so we don't have to recurse!
for card_idx in range(len(card_list)-1, -1, -1) :

    card_id = card_list[card_idx]
    this_card_won = calc_points(all_cards_winning[card_id], all_cards_havenum[card_id], )

    # look to see how many cards each of the copies it wins gives us
    this_total = 1
    for next_idx in range(this_card_won) :
        tmp_idx = card_idx + next_idx + 1
        if len(card_list) > tmp_idx and card_list[tmp_idx] in cards_won_by_card :
            this_total += cards_won_by_card[card_list[tmp_idx]]
    cards_won_by_card[card_id] = this_total

print("Cards won by card: " + str(cards_won_by_card))

# to get our overall total, just sum up what we got from 1 of each card
tot_cards = 0
for card_id in cards_won_by_card :
    tot_cards += cards_won_by_card[card_id]

print("Total cards: " + str(tot_cards))

"""
With my inputs: 9924412

Time for part 1: 0:20
Time for part 2: 0:45
Total time for this day: 1:05
"""