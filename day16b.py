#!/usr/bin/env python
# encoding: utf-8
"""
By Eva Schiffer
for the advent of code
"""

import re, sys, os

import numpy

NUMBERS_EXP = r'-?\d+' # ? is zero or 1 times, so numbers can now be negative

#print("Args: " + str(sys.argv))
day_txt = re.findall(NUMBERS_EXP, os.path.basename(vars(sys.modules[__name__])['__file__']))[0]
file_to_open = "./input/day"+day_txt+"_input_"+sys.argv[1]+".txt" if len(sys.argv) > 1 else "./input/day"+day_txt+"_input.txt"
file_obj = open(file_to_open)
data = file_obj.readlines()

# Parse input to find
#       a map that reprsesents a rectangular mirror board

"""
    '.' is empty space
        light passes right through
    '/' and '\' are diagonal mirrors
        light makes a 90 degree bounce when hitting a diagonal mirror
    '|' and '-' are splinters
        light ignores the spliter if it hits on end
        light splits into two 90 degree angles if it hits flat side
    
    all tiles that have beams enter them are "energized"
"""

# parse our input
mirror_map = numpy.zeros((len(data), len(data[0].strip())), dtype=numpy.str_)
# step through all the lines
line_idx = 0
while line_idx < len(data) :
    mirror_map[line_idx,:] = list(data[line_idx].strip())
    line_idx += 1

def print_map(map_array, indent=0, true_char='#', false_char='.', extra_stuff_positions=None, ) :
    """
    Given a map info that is a string or bool, print it in a user readable way.
    :param map_array:
    :param indent:
    :param true_char:
    :param false_char:
    :param extra_stuff_positions:
    :return:
    """
    extra_stuff_positions = set( ) if extra_stuff_positions is None else extra_stuff_positions

    indent_txt = ""
    for i in range(indent) :
        indent_txt += "\t"

    for l_idx in range(map_array.shape[0]) :
        this_line = indent_txt
        for c_idx in range(map_array.shape[1]) :
            if map_array.dtype == bool :
                tmp_char = true_char if map_array[l_idx,c_idx] else false_char
                                                                                        # invert extra stuff
                tmp_char = tmp_char if (l_idx,c_idx) not in extra_stuff_positions else "\033[7m" + tmp_char + "\033[0m"
                this_line += tmp_char
            else :
                tmp_char = str(map_array[l_idx,c_idx])
                                                                                        # invert extra stuff
                tmp_char = tmp_char if (l_idx, c_idx) not in extra_stuff_positions else "\033[7m" + tmp_char + "\033[0m"
                this_line += tmp_char
        print(this_line)

# visualize input to make sure we parsed it correctly
do_print = True
if do_print :
    print("Input mirror map: ")
    print_map(mirror_map, indent=1,)

NORTH = "north"
SOUTH = "south"
EAST  = "east"
WEST  = "west"

TO_GO = {
            NORTH:  (-1,  0),
            SOUTH:  ( 1,  0),
            WEST:   ( 0, -1),
            EAST:   ( 0,  1),
        }

DIR_MAP =   {
                '.':    {NORTH: NORTH,
                         SOUTH: SOUTH,
                         WEST: WEST,
                         EAST: EAST,},
                '/':    {NORTH: EAST,
                         SOUTH: WEST,
                         WEST: SOUTH,
                         EAST: NORTH,},
                '\\':   {NORTH: WEST,
                         SOUTH: EAST,
                         WEST: NORTH,
                         EAST: SOUTH,},
                '|':    {NORTH: NORTH,
                         SOUTH: SOUTH,
                         WEST: (NORTH, SOUTH),
                         EAST: (NORTH, SOUTH),},
                '-':    {NORTH: (EAST, WEST),
                         SOUTH: (EAST, WEST),
                         WEST: WEST,
                         EAST: EAST,},
            }

def is_within_rect(l_idx, c_idx, shape, ) :
    """
    Is the given set of indexes within the shape?
    :param l_idx:
    :param c_idx:
    :param shape:
    :return:
    """
    #print("Checking (" + str(l_idx) + ", " + str(c_idx) + ") against shape: " + str(shape))

    return (l_idx >= 0) and (l_idx < shape[0]) and (c_idx >= 0) and (c_idx < shape[1])

def move_light (current_beams, energized_tiles, mirror_map,) :
    """
    Given a set of input beams, a set of which tiles are energized, and the mirror map,
    move the light around the board keeping track of where it's been.
    :param current_beams:
    :param energized_tiles:
    :param mirror_map:
    :return:
    """

    already_cache = set( )

    tmp_beams = current_beams.copy()

    while len(tmp_beams) > 0 :
        #print ("tmp beams: " + str(tmp_beams))
        next_beams = set()
        already_cache = already_cache.union(tmp_beams)
        for tmp_beam in tmp_beams :
            start_dir = tmp_beam[2]
            next_space = (tmp_beam[0] + TO_GO[start_dir][0], tmp_beam[1] + TO_GO[start_dir][1])
            #print("next space: " + str(next_space))
            # if the beam goes off the map we can stop caring about it
            if is_within_rect(next_space[0], next_space[1], mirror_map.shape,) :
                next_dir = DIR_MAP[mirror_map[next_space]][start_dir]
                energized_tiles.add(next_space)
                if len(next_dir) == 2 :
                    tmp = (next_space[0], next_space[1], next_dir[0],)
                    if tmp not in already_cache :
                        next_beams.add(tmp)
                    tmp = (next_space[0], next_space[1], next_dir[1],)
                    if tmp not in already_cache :
                        next_beams.add(tmp)
                else : # there should be only one direction to go
                    tmp = (next_space[0], next_space[1], next_dir,)
                    if tmp not in already_cache :
                        next_beams.add(tmp)
        tmp_beams = next_beams
        #print("tmp beams: " + str(tmp_beams))

        #print ("After next move of light:")
        #print_map(mirror_map, indent=1, extra_stuff_positions=energized_tiles, )

def generate_all_edge_beams(map_shape, ) :
    """
    Given that we can fire beams in from all the edges, what are possible starting beams?
    :param map_shape:
    :return:
    """
    all_beams = set( )

    # make all west and east row beams
    for l_idx in range(map_shape[0]) :
        all_beams.add((l_idx, -1,           EAST),)
        all_beams.add((l_idx, map_shape[1], WEST),)

    # make all the north and south column beams
    for c_idx in range(map_shape[1]) :
        all_beams.add((-1,           c_idx, SOUTH),)
        all_beams.add((map_shape[0], c_idx, NORTH),)

    return all_beams

# see how the light travels so we can figure out where it goes
tmp_all_possible_beams = generate_all_edge_beams(mirror_map.shape,)
max_energized = 0
best_energized = None
best_beam = None
for tmp_beam in tmp_all_possible_beams :
    print("Testing beam: " + str(tmp_beam))
    tmp_energized_tiles = set( )
    move_light({tmp_beam,}, tmp_energized_tiles, mirror_map,)
    this_num_e_tiles = len(list(tmp_energized_tiles))
    if this_num_e_tiles > max_energized :
        print("Found new best beam.")
        max_energized = this_num_e_tiles
        best_beam = tmp_beam
        best_energized = tmp_energized_tiles

print("Best energized map (starting at " + str(best_beam) + "): ")
print_map(mirror_map, indent=1, extra_stuff_positions=best_energized,)

print("Best number of energized tiles: " + str(max_energized))

"""
With my inputs: 8231

Time for part 1: 1:04 
Time for part 2: 0:12
Total time for this day: 1:16
"""