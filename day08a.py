#!/usr/bin/env python
# encoding: utf-8
"""
By Eva Schiffer
for the advent of code
"""

import re, sys
import numpy

#print("Args: " + str(sys.argv))
day_txt = "8"
file_to_open = "./input/day"+day_txt+"_input_"+sys.argv[1]+".txt" if len(sys.argv) > 1 else "./input/day"+day_txt+"_input.txt"
file_obj = open(file_to_open)
data = file_obj.readlines()

#NUMBERS_EXP = r'\d+'
NODE_NAME_EXP = r'[A-Z]{3}'

# Parse input to find
#       a set of LR moves
#       a simple graph with two connections (left and right) per node

# parse our input
paths = { }
moves_str = data[0].strip()
line_idx = 2 # start on the third line
while line_idx < len(data) and len(data[line_idx]) > 2 :
    tmp = re.findall(NODE_NAME_EXP, data[line_idx],)
    paths[tmp[0]] = (tmp[1], tmp[2],)   # [0] in the tuple is the left path, [1] is the right path
    line_idx += 1

print("Moves: " + moves_str )
print("Paths: " + str(paths))

def make_a_move (current_node, move_index, moves_str, paths_map,) :
    """
    Given where we are, where we are in the string of moves, the moves string, and the map of paths
    take one more move along the path.
    :param current_node:
    :param move_index:
    :param moves_str:
    :param paths_map:
    :return: The new current node and move index.
    """

    move_to_take = moves_str[move_index]
    new_move_idx = (move_index + 1) % len(moves_str)    # rollover when we get to the end of the moves string

    tmp_paths = paths_map[current_node]
    next_node = tmp_paths[0] if move_to_take == "L" else tmp_paths[1]

    return next_node, new_move_idx

# figure out how many moves it takes to get from AAA to ZZZ
num_moves = 0
move_idx = 0
curr_node = "AAA"
while curr_node != "ZZZ" :
    print("Taking move " + str(num_moves + 1))
    curr_node, move_idx = make_a_move(curr_node, move_idx, moves_str, paths,)
    print("New node: " + curr_node)
    num_moves += 1

print("Moves needed: " + str(num_moves))

"""
With my inputs: 20513

Time for part 1: 0:25 
"""